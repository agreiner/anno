#!/usr/bin/env python

# anno.py, a script to accept a user query of HPC annotations relating to logfiles. Intended to be integrated into LogSet.
# Note: this script assumes the user has full permissions on the database. It would be insecure for use by untrusted users (e.g., on the open web).

import argparse
import sys
import sqlite3
import json
import datetime
from dateutil import parser as dateparser
import re
import logging

#defaults
annodb = 'annotations.sqlite3'
jobdb = 'jobs.sqlite3'

try:
    config=open('anno.conf', 'r')
    for line in config:
        m = re.match('anno_db_path=\'([^=]*)\'', line.lstrip())
        if m:
            annodb = m.group(1)
        m = re.match('job_db_path=\'([^=]*)\'', line.lstrip())
        if m:
            jobdb = m.group(1)
    
except IOError:
    print('No anno.conf paths found. Using defaults.')

def expand_nidlist(nlist):
    """ translate a nodelist like 'nid[02516-02575,02580-02635,02836]' into a 
        json dump of explicitly-named nodes, eg '["nid02516","nid02517", ...]'
    """
    nodes = []
    prefix, sep0, nl = nlist.partition('[')
    if sep0:
        for component in nl.rstrip(']').split(','): 
            first,sep1,last = component.partition('-')
            if sep1:
                nodes += [ 'nid{:05d}'.format(i) for i in range(int(first),int(last)+1) ]
            else:
                nodes += [ 'nid{:05d}'.format(int(first)) ]
    else:
        nodes += [ prefix ]
    return json.dumps(nodes)
    
def expand_numlist(nlist):
    """ translate a nodelist like '02516-02575,02580-02635,02836' into a 
        json dump of explicitly-named nodes, eg '["nid02516","nid02517", ...]'
    """
    nodes = []
    for component in nlist.lstrip('\'[').rstrip(']\'').split(','):
        first,sep,last = component.partition('-')
        if sep:
            nodes += [ 'nid{:05d}'.format(i) for i in range(int(first),int(last)+1)]
        else:
            nodes += [ 'nid{:05d}'.format(int(first)) ]
    return json.dumps(nodes)


def filter_anns(componentlist, annotations):
# filter annotations to keep only annotations for the components in the list
    filtered_anns = []
    for ann in annotations:
        if ann[14] != None:
            comps_in_ann = json.loads(ann[14])
            for component in componentlist:
                if component in comps_in_ann:
                    filtered_anns.append(ann)
                    break
    return filtered_anns


def get_parents(component, system, depth):
    if depth < 1:
        return []
    #hit the db
    arch_connn = sqlite3.connect(annodb)
    arch_cur = arch_connn.cursor()
    arch_query = "select parent from physicalcomponents where cname='" + component + "'"
    if system != None:
        arch_query += " and system='" + system + "'"
    arch_cur.execute(arch_query)
    parents = []
    depth-=1
    result = arch_cur.fetchone()
    if result != None:
        result_parent = result[0]
        if result_parent != None:
            parentlist = json.loads(result_parent)
            parents.extend(parentlist)
            if depth > 0:
                parents.extend(get_parents(result_parent, system, depth))
    return parents


def get_children(component, system, depth):
    if depth < 1:
        return []
    #hit the db
    arch_connn = sqlite3.connect(annodb)
    arch_cur = arch_connn.cursor()
    arch_query = "select children from physicalcomponents where cname='" + component + "'"
    if system != None:
        arch_query += " and system='" + system + "'"
    arch_cur.execute(arch_query)
    children = []
    depth-=1
    result = arch_cur.fetchone()
    if result != None:
        result_children = result[0]
        if result_children != None:
            childrenlist = json.loads(result_children) # need to parse json
            children.extend(childrenlist)
            if depth > 0:
                for child in childrenlist:
                    children.extend(get_children(child, system, depth))
    return children


def get_relations(args, system=None):
    component = args.component
    depth = int(args.depth)
    relations = []

    # include the smw unless the user requested otherwise
    if not args.nosmw:
        relations.extend(["smw"])
    # include annotations with unknown components if the user requested they be included
    if args.nullc:
        relations.extend(["", None])

    # get parents
    comps_parents = get_parents(component, system, depth)
    print("parents: " + str(comps_parents))
    # get children
    comps_children = get_children(component, system, depth)
    print("children: " + str(comps_children))
    print("")
    relations.extend(comps_parents)
    relations.extend(comps_children)
    # make unique
    relations = list(set(relations))
    # return the list of all related components
    return relations

def run_sql(db, query):
    if db == 'jobs':
        db_to_use = jobdb
    else:
        db_to_use = annodb
    try:
        connn = sqlite3.connect(db_to_use)
        cur = connn.cursor()
    except:
        print("Unable to connect to the specified database.")
        return
    try:
        cur.execute(query)
    except:
        print("Unable to run the query on the specified database.")
    
    for row in cur:
        print(row)


def print_anns(anns_out, headers, format=None):
    if format == 'table':
        for ann in anns_out:
            ann_str = '\t'.join(str(e) for e in ann)
            print(ann_str)

    elif format == 'json':
        outputlist = []
        for ann in anns_out:
            outputdict = {}
            for i in range (len(headers)-1):
                outputdict[headers[i]] = ann[i]
            outputlist.append(outputdict)
        print(json.dumps(outputlist))

    else:
        if len(anns_out) == 0:
            print('No annotations found')
        for ann in anns_out:
            print('----------')
            print('[' + str(ann[headers.index('id')]) + ']', 'by', ann[headers.index('authorid')], 'on system', ann[headers.index('system')])
            print('Time:', ann[headers.index('starttime')],'to', ann[headers.index('endtime')])
            print('Start state:', ann[headers.index('startstate')], '; End state:',  ann[headers.index('endstate')])
            print('Description:', ann[headers.index('description')])
            print('Manually invoked?', bool(ann[headers.index('manual')]), '; System down?: ',  bool(ann[headers.index('systemdown')]))
            print('Components:', ann[headers.index('components')])
            print('Tags:')
            if ann[headers.index('LDcatgroup')]:
                print ('LogDiver category group:', ann[headers.index('LDcatgroup')])
            if ann[headers.index('LDcategory')]:
                print ('LogDiver category:', ann[headers.index('LDcategory')])
            if ann[headers.index('LDtag')]:
                print ('LogDiver tag:', ann[headers.index('LDtag')])
            if ann[headers.index('balerpatternid')]:
                print ('Baler pattern ID:', ann[headers.index('balerpatternid')])
            print('Relevant log files:', ann[headers.index('logfiles')])


# test the db connections. db is either annodb or jobdb
def make_db_conn(db):
    try:
        db_connn = sqlite3.connect(db)
        db_cur = db_connn.cursor()
        query = 'select * from ' + ('annotations' if db==annodb else 'jobs') + ' limit 1'
        db_cur.execute(query)
        return(db_cur)
    except:
        print("Unable to connect to the database at " + db)
        return(False)


def main():
    parser = argparse.ArgumentParser(description="Query the annotations for a logset.")
    parser.add_argument('-s', '--starting', help="A start time that limits retrieved annotations to those relevant to the time at or after it, e.g. '2017-01-01 12:00:00'. When used in conjunction with --jobid, overrides the job start time.")
    parser.add_argument('-e', '--ending', help="An end time that limits retrieved annotations to those relevant to the time at or before it, e.g. '2017-01-01 12:59:59'. When used in conjunction with --jobid, overrides the job end time.")
    parser.add_argument('-c', '--component', help="A component identifier. When used in conjunction with --jobid, overrides the job's list of components.")
    parser.add_argument('-t', '--type', help="A string describing a type of event")
    parser.add_argument('-n', '--next', help="A time after which to find the next single annotation with a start time after the given time and that otherwise matches the query, e.g. '2017-01-01 12:00:00'")
    parser.add_argument('-j', '--jobid', help="A job id from the scheduler")
    parser.add_argument('-f', '--format', help="How to format the output, table, json, or default")
    parser.add_argument('-d', '--depth', help="How many levels to traverse to find related components. This has no effect without a component or job flag. The system management workstation is included by default.")
    parser.add_argument('-v', '--verbose', help="Be noisier. -vv is noisier than -v, etc", action='count')
    parser.add_argument('-q', '--sql', help="A database and an sql query to run against it. The first argument is the database ('jobs' or 'annotations'). The second is the query, in single quotes. Use double quotes to delimit strings within the query.", nargs=2)
    parser.add_argument('-l', '--limit', help="Limit the number of output records")
    parser.add_argument('--jobs', help="Show which jobs were running during the time spanned by this annotation. Requires annotator initials and annotation id, e.g., AMG123.")
    parser.add_argument('--nosmw', action='store_true', help='Override default addition of system management workstation as a component in component queries with depth specified')
    parser.add_argument('--nullc', action='store_true', help='Include annotations that do not contain a component in results from component queries with depth specified')
    parser.add_argument('-a', '--all', action='store_true', help='Return all annotations in the database (may be very large)')
    parser.add_argument('-y', '--system', help="A system name to which the query should be restricted. If not specified, results will be returned for all systems in the database.")
    args = parser.parse_args()

    #anns_or_logs = args.output

    # if the user supplies an sql query, just run it against the specified database and forget about retrieving annotations

    if len(sys.argv)==1:
        parser.print_help()
        return

    if args.starting and args.next:
        print("You cannot use --starting and --next at the same time")
        return

    if args.ending and args.next:
        print("You cannot use --ending and --next at the same time")
        return

    if args.sql:
        db = args.sql[0]
        query = args.sql[1]
        run_sql(db, query)
        return

    where_clause = ""
    order_clause = "order by starttime"
    limit_clause = ""
    componentlist = []
    timeformat = '%Y-%m-%d %H:%M:%S'
    system = None

    if args.verbose:
        logger = logging.getLogger()
        level = logger.getEffectiveLevel() - 10*args.verbose
        logger.setLevel(level)

    anns_cur = make_db_conn(annodb)
    jobs_cur = make_db_conn(jobdb)


    # get names of columns for handling annotation query results,
    ann_column_names = [i[0] for i in anns_cur.description]
    headerstr = '\t'.join(ann_column_names)
    if args.format=='table':
        print(headerstr)

    if args.limit:
        limit_clause = ' limit {0} '.format(args.limit)


    # if a jobid is given, get the nodes, start and end for the job, then set the
    # component, start, and end parameters.
    if args.jobid:
        if not re.match('\d+', args.jobid):
            print('For a job query, please provide a numeric identifier for the job')
            return
        jobs_cur.execute("select * from jobs where jobid=" + args.jobid)
        jobs_column_names = [e[0] for e in jobs_cur.description]
        jobinfo = jobs_cur.fetchone()
        if jobinfo != None:
            started = jobinfo[jobs_column_names.index('Start')]
            finished = jobinfo[jobs_column_names.index('End')]
            nodeliststr = jobinfo[jobs_column_names.index('NodeList')]
            system = jobinfo[jobs_column_names.index('system')]
            if not re.match('\[\"c.*\"', nodeliststr):
                if 'nid' in nodeliststr:
                    nodeliststr = expand_nidlist(nodeliststr)
                else:
                    nodeliststr = expand_numlist(nodeliststr)
            print('Job {} started at {} and finished at {}.'.format(args.jobid, started, finished))
            print('It ran on the following {} nodes: {}'.format(system, nodeliststr))
            #use the start and end time of the job unless they were given by the user
            if not args.starting:
                args.starting = started # when the job started
            if not args.ending:
                args.ending = finished # when the job ended
            if not args.component:
                # ignore component list if user specified a single component
                componentlist = json.loads(nodeliststr)
        else:
            print('No job found.')
            return
        extrajob = jobs_cur.fetchone()
        if extrajob != None:
            print('Found another job with the same id. Use the -y option to select a specific system.')

    # find jobs that ran during the time of an annotation
    if args.jobs:
        if not re.match('[A-Za-z]{3}\d+', args.jobs):
            print('For a jobs query, please provide an annotation identifier consisting of the ' +
                'three-letter author id (case sensitive) followed by the numeric annotation id (no spaces).')
            return
        ann_id = args.jobs[3:]
        ann_auth = args.jobs[0:3]
        where_clause = " where id={0} and authorid='{1}'".format(ann_id, ann_auth)                   
        prefix = 'select starttime, endtime, system from annotations'
        timequery = "{0} {1}".format(prefix,where_clause)
        anns_cur.execute(timequery)
        ann_found = anns_cur.fetchone()
        if ann_found is None:
            print('annotation {0} not found'.format(ann_id))
            return
        ann_start = ann_found[0]
        ann_end = ann_found[1]
        ann_sys = ann_found[2]
        order_clause = ' order by start'
        prefix = 'select * from jobs'
        limit_clause = ' limit 50'
        # the limit here is because bad entries in db lead to zillions of results printed
        where_clause = " where Start<='{1}' and End>='{0}'".format(ann_start, ann_end)
        if ann_sys != None:
            where_clause += "and system='{0}'".format(ann_sys)
        query = "{0} {1} {2} {3}".format(prefix,where_clause,order_clause,limit_clause)
        logging.debug("running query: {0}".format(query))
        jobs_cur.execute(query)
        jobs_found = jobs_cur.fetchall()
        if len(jobs_found)>0:
            for j in jobs_found:
                    print(j)
        else :
            print('No jobs found.')
        return

    # if a start time is given, find annotations with time ranges that overlap the time
    # after it. That is, with end times that are after it (so we catch stuff that started
    # before but ended after.)
    if args.starting:
        # try to guess the time format and convert it to what SQLite3 understands
        try:
            starting_datetime = dateparser.parse(args.starting)
        except:
            print("Please enter a start time in a standard format, e.g., '2017-01-01 12:00:00'")
            return
        formatted_starting = starting_datetime.strftime(timeformat)
        if where_clause != "":
            where_clause += " and"
        else:
            where_clause += " where"
        where_clause += " endtime >= '" + formatted_starting + "'"

    # if an end time is given, find annotations with time ranges that overlap the time
    # before it. That is, with start times that are before it (so we catch stuff that
    # started before and ended after.)
    if args.ending:
        try:
            ending_datetime = dateparser.parse(args.ending)
        except:
            print("Please enter an end time in a standard format, e.g., '2017-01-01 12:00:00'")
            return
        formatted_ending = ending_datetime.strftime(timeformat)
        if where_clause != "":
            where_clause += " and"
        else:
            where_clause += " where"
        where_clause += " starttime <= '" + formatted_ending + "'"

    # if both a start and end are given, find annotations whose time ranges overlap them.
    # This is already handled above. That is, we want annotations with start times before
    # the end time and end times after the start time.

    # if the user specified a specific system, retrieve annotations for that one only
    if args.system:
        system = args.system
    if system != None: # might also have been set with a jobid query above
        if where_clause != "":
            where_clause += "and"
        else:
            where_clause += "where"
        where_clause += " system='{0}'".format(system)


    # if a component is given, query for annotations that apply to that component.
    if args.component and not re.match('smw|c\d-\d.*', args.component):
        anns_cur.execute("select name from alias where aliases like '%\"{0}\"%'".format(args.component))
        cname = anns_cur.fetchone()
        if cname != None:
            print("{0} is an alias for {1}, querying for {1}".format(args.component, cname[0]))
            args.component = cname[0]
        else:
            print("Sorry, that component name is not recognized.")


    if args.component and not args.depth:
        if where_clause != "":
            where_clause += " and"
        else:
            where_clause += " where"
        where_clause += " components like '%\"{0}\"%' ".format(args.component)

    # if a component and levels are given, query for the component and each related component
    if args.component and args.depth:
        if not re.match('\d+', args.depth):
            print("Please enter a depth value that is a positive integer")
            return
        componentlist = [args.component]
        relations = get_relations(args, system=system)
        componentlist.extend(relations)

    # if a type of event is specified, query for annotations with the phrase in
    # their description or endstate or a logdiver tag
    if args.type:
        if where_clause != "":
            where_clause += " and"
        else:
            where_clause += " where"
        col,sep,pattern = args.type.lower().rpartition('=')
        cols = ('description', 'endstate', 'ldcatgroup', 'ldcategory', 'ldtag')
        cols_short = [c[:3] for c in cols]
        if col[:3] in cols_short:
            # find the actual column it matches
            for c in range(len(cols)):
                if cols[c].startswith(col):
                    actual_col = cols[c]
                    break
            else:
                raise Exception("can't find a column matching {0}".format(col))
            where_clause += " {0} like '%{1}%' ".format(actual_col,pattern)
        else:
            where_clause += " ( " + " or ".join([" {0} like '%{1}%' ".format(col,args.type) for col in cols]) + " ) "
        #where_clause += " (description like '%" + args.type + "%' or endstate like '%" + args.type + "%' or ldtag like '%" + args.type + "%')"

    # if an "after" timestamp is given, set the where clause to use it as a lower
    # bound on the start time, and only retrieve the first one after that time.
    # This finds only the first instance that *started* after the time given.
    if args.next:
        try:
            after_datetime = dateparser.parse(args.next)
        except:
            print("Please enter a time in a standard format, e.g., '2017-01-01 12:00:00'")
            return

        formatted_after = after_datetime.strftime(timeformat)
        if where_clause != "":
            where_clause += " and"
        else:
            where_clause += " where"
        limit_clause = 'limit 1'
        where_clause += " starttime > '" + formatted_after + "'"

    #logfile requests - not working yet
    #if a logseries is given, only look for logs in that series
    #if args.logseries:
    #    pass

# do this:
# sqlite3 jobs.sqlite3 "create index starttime on jobs (start asc) ;"
# sqlite3 jobs.sqlite3 "create index endtime on jobs (end asc) ;"
# sqlite3 annotations.sqlite3 "create index start on annotations (starttime asc) ;"
# sqlite3 annotations.sqlite3 "create index end on annotations (endtime asc) ;"

    # run the queries
    prefix = 'select * from annotations'
    #suffix = ' order by starttime'
    if len(componentlist) > 0:
        # run a query for each component, if there are more than one. 
        # We don't run them all into a single query because they can easily 
        # exceed the limit of query expressions.
        comp_count=0
        for component in componentlist:
            comp_count+=1
            if comp_count%100 == 0:
                print("checked " + str(comp_count) + " components")
            new_clause = where_clause
            if new_clause != "":
               new_clause += "and"
            else:
               new_clause += "where"
            new_clause += " components like '%\"{0}\"%' ".format(component)
            query = "{0} {1} {2} {3}".format(prefix,new_clause,order_clause,limit_clause)
            logging.debug("Executing query: {0}".format(query))
            anns_cur.execute(query)
            anns_found = anns_cur.fetchall()
            if len(anns_found) > 0:
               print(component)
               print_anns(anns_found, ann_column_names, args.format)
        print("*** Done! ***")

    else:
        # run one query on the db
        #query = prefix + where_clause + suffix
        query = "{0} {1} {2} {3}".format(prefix,where_clause,order_clause,limit_clause)
        logging.debug("Executing query: {0}".format(query))
        anns_cur.execute(query)
        anns_found = anns_cur.fetchall()
        print_anns(anns_found, ann_column_names, args.format)

main()
