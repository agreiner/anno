# environment for testing anno.py
from behave import fixture, use_fixture
import re
import sqlite3

import sys
sys.path.append('.')

import logging
logging.getLogger().setLevel(logging.DEBUG)

@fixture
def db_annotations(context, timeout=30):
    #defaults
    annodb = 'annotations.sqlite3'

    try:
        config=open('anno.conf', 'r')
        for line in config:
            m = re.match('anno_db_path=\'([^=]*)\'', line.lstrip())
            if m:
                annodb = m.group(1)
        
    except IOError:
        pass

    annoconn = sqlite3.connect(annodb)
    context.anno_cursor = annoconn.cursor()
    yield context.anno_cursor
    annoconn.close()

def before_tag(context, tag):
    if tag == "fixture.db.annotations":
        use_fixture(db_annotations, context)
    if tag == "fixture.db.jobs":
        use_fixture(db_jobs, context)

@fixture
def db_jobs(context, timeout=30):
    #defaults
    jobdb = 'jobs.sqlite3'

    try:
        config=open('anno.conf', 'r')
        for line in config:
            m = re.match('job_db_path=\'([^=]*)\'', line.lstrip())
            if m:
                jobdb = m.group(1)
        
    except IOError:
        pass

    jobconn = sqlite3.connect(jobdb)
    context.job_cursor = jobconn.cursor()
    yield context.job_cursor
    jobconn.close()

import os
def remove(path):
    try:
        os.remove(path)
    except FileNotFoundError:
        pass

def remove_ingest_testing_debris():
    for path in ['test-alias-template.csv', 'test-annotations.csv', 'test-annotations-template.csv',
                 'test-authors-template.csv', 'test-physicalcomponents-template.csv', 
                 'test-metaannotations-template.csv', 'test-jobs.csv',
                 '../sample-data/annotations-test.sqlite3', '../sample-data/jobs-test.sqlite3']:
        remove(path)
    
import random, string
def ran_str(len):
    return ''.join([random.choice(string.ascii_lowercase) for i in range(len)])

def before_feature(context, feature):
    if feature.name == 'ingesting annotations':
        context.anno_conf_backup = f"anno.conf.bak-{ran_str(5)}"
        os.rename('anno.conf', context.anno_conf_backup)
        remove_ingest_testing_debris()

def after_feature(context, feature):
    if feature.name == 'ingesting annotations':
        remove('anno.conf')
        os.rename(context.anno_conf_backup, 'anno.conf')
        if feature.status == "passed":
            remove_ingest_testing_debris()

