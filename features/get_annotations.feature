Feature: Get annotations

As a researcher

I want to retrieve all annotations that meet my search criteria


@fixture.db.annotations
Scenario: Retrieve annotations for a single component

        Given the annotations database has at least one entry for the component of interest

        When I enter the component name at the command line

        Then the results should include at least one annotation that applies to that component 

        And the results contain no annotations that do not apply to that component


@fixture.db.annotations
Scenario: Retrieve annotations that apply to a time after a given timestamp

        Given the annotations database has at least one entry with an end time later than the given timestamp

        When I enter a start time at the command line

        Then the results should include at least one annotation with an end time that is after the given timestamp and none that has an end time before the given timestamp


@fixture.db.annotations
Scenario: Retrieve annotations that apply to a time before a given timestamp

        Given the annotations database has at least one entry with a start time before the given timestamp

        When I enter an end time at the command line

        Then the results should include at least one annotation with a start time that is before the given timestamp and none that have a start time after the given timestamp


@fixture.db.annotations
Scenario: Retrieve annotations timestamped during a given time range

        Given the annotations database has at least one entry for the time range of interest

        When I enter a start time and an end time at the command line

        Then the results should include at least one annotation with a start time before the entered end time and an end time after the entered start time, and no annotations that have a start time after the entered end time or an end time before the entered start time


@fixture.db.annotations
Scenario: Retrieve annotations that contain a given string

        Given the annotations database has at least one entry that contains the given string

        When I enter a request for a type with the string at the command line

        Then the results should include at least one entry that contains the string and no entries that do not contain the string


@fixture.db.annotations
@fixture.db.jobs
Scenario: Retrieve annotations for a job

        Given the annotations database has an entry for at least one node on which the job ran at the time the job ran

        When I enter the job id at the command line

        Then the results should include at least one annotation that applies to the time during which the job ran and that involves a node on which the job ran, and no results that only apply outside the time range or to nodes on which the job did not run


@fixture.db.annotations
Scenario: Retrieve annotations in tabular format

        Given  the annotations database has at least one entry for a component of interest

        When I enter the component name at the command line and specify tabular output

        Then the results should include at least one annotation formatted as a table


@fixture.db.annotations
Scenario: Retrieve annotations in json format

        Given the annotations database has at least one entry for another component of interest

        When I enter the component name at the command line and specify json output

        Then the results should include at least one annotation formatted as json


@fixture.db.annotations
Scenario: Retrieve no more than a given number of records

        Given the annotations database has at least ten entries

        When I enter a request for all entries with a limit of 2

        Then the results should include exactly 2 entries


@fixture.db.annotations
Scenario: Retrieve annotations for components a given number of hops apart

        Given the annotations database has at least one entry for a component that is one hop from a particular component and one that is two hops from it

        When I enter the particular component id at the command line and specify two hops

        Then the results should include at least one annotation that applies to the component one hop away and one for the component that is two hops away


@fixture.db.annotations
@fixture.db.jobs
Scenario: Retrieve jobs that correspond to a specific annotation

        Given the annotations database has at least one entry for a job that ran during the time of a specific annotation and on the same machine

        When I enter the identifier for an annotation at the command line and specify jobs

        Then the results should include at least one job that was running on that machine during the time range of the annotation.


@fixture.db.annotations
Scenario: Retrieve the first annotation that has a start time after a given datetime

        Given the annotations database has at least one entry with a start time after the given time

        When I enter a request for the next annotation and enter a datetime at the command line

        Then the results should include one entry that has a starttime after the given time
