#!/usr/bin/env python3

import sys
sys.path.append("..")
from ingest_annotations.db_support import connect_anno_db, connect_jobs_db, create_anno_db, create_jobs_db, read_paths
from ingest_annotations.db_support import connect_all, write_csv_template, ingest_csv, session_context
from ingest_annotations.db_support import Annotation, Author, Alias, Job
import logging

# Background: databases have been nominated

@given('anno.conf contains the text')
def set_anno_conf(context):
    text = getattr(context, "text", None)
    with open('../anno.conf', 'w') as f:
        f.write(getattr(context, "text", None))
    context.anno_db_path, context.jobs_db_path = read_paths()


# Scenario Outline: creating and using annotations databases

import os
def remove(path):
    try:
        os.remove(path)
    except FileNotFoundError:
        pass

@given('the annotations db exists {yes_or_no}')
def set_annodb_existence(context, yes_or_no):
    if yes_or_no == 'yes':
        print("creating anno db")
        create_anno_db(context.anno_db_path, force=True)
        check_databases(context)
    else:
        print("removing anno db")
        remove(context.anno_db_path)

@given('the jobs db exists {yes_or_no}')
def set_jobsdb_existence(context, yes_or_no):
    if yes_or_no == 'yes':
        print("creating jobs db")
        create_jobs_db(context.jobs_db_path, force=True)
    else:
        print("removing jobs db")
        remove(context.jobs_db_path)

@when('db connections are made')
def connect_dbs(context):
    context.anno_session = connect_anno_db(context.anno_db_path)
    context.jobs_session = connect_jobs_db(context.jobs_db_path)

import sqlite3
def schema_ok(db, entity, expected_set):
    with db:
        cursor = db.cursor()
        query = f"select name from sqlite_master where type='{entity}'"
        cursor.execute(query)
        entities = { row[0] for row in cursor }
    retval = entities >= expected_set
    if not retval:
        logging.warning(f"expected to find {expected_set} \nbut found {entities}")
        logging.warning(f"unexpected-but-found: {entities-expected_set}")
        logging.warning(f"expected-but-unfound: {expected_set-entities}")
    return retval

@then('both databases exist and have the correct schemas')
def check_databases(context):
    print("checking databases")
    db = sqlite3.connect(context.anno_db_path)
    expected_tables = {'SUP', 'linktypes', 'alias', 'LDgroups', 'metaannotations',
                       'link', 'architectures', 'authors', 'annotations', 'rtr',
                       'physicalcomponents', 'associationtypes', 'componenttypes'}
    assert schema_ok(db, 'table', expected_tables)

    # sqlalchemy names the indexes according to a convention, so testing the index 
    # names is too fragile. TODO come up with a better test
    #expected_indexes = {'start', 'end' }
    #assert schema_ok(db, 'index', expected_indexes)

    expected_triggers = {'increment_id', 'increment_metaid'}
    assert schema_ok(db, 'trigger', expected_triggers)

    db = sqlite3.connect(context.jobs_db_path)
    expected_tables = {'jobs'}
    assert schema_ok(db, 'table', expected_tables)


# Scenario: providing csv templates for user to add annotations and jobs
# importing globals from another module doesn't work (not sure why?)
#from ingest import AnnoSessionManager, JobsSessionManager

@given('a {table} has been nominated')
def nominate_a_table(context, table):
    context.chosen_table = table

@given('the dbs are connected')
def connect_dbs(context):
    connect_all()

@when('write_csv_template is called')
def call_make_template(context):
    context.csvname = f'test-{context.chosen_table}-template.csv'
    write_csv_template(context.chosen_table, context.csvname)

@then('a csv file for that {table} is produced')
def check_csv_template(context, table):
    assert os.path.isfile(context.csvname)
    # TODO should check that the file contents are sensible ..

# Scenario: ingesting some annotations

@given('the following {table} in a csv file')
def make_dummy_csv_file(context, table):
    context.dummycsvname = f'test-{table}.csv'
    with open(context.dummycsvname, 'w') as csvfile:
        csvfile.write(','.join(context.table.headings) + '\n')
        for row in context.table:
            csvfile.write(','.join(row) + '\n')

@given('the alias table has entries like')
def add_alias(context):
    with session_context() as session:
        for row in context.table:
            session.add(Alias(name=row['name'], aliases=row['aliases']))


def annotations(context, since:int=0):
    with session_context() as session:
        anns = session.query(Annotation).filter(Annotation.id>since)
        for entry in anns:
            yield entry

import sqlalchemy as sql
def max_annotation_id(context):
    with session_context() as session:
        return session.query(sql.sql.expression.func.max(Annotation.id)).scalar() or -1

@given('the maximum id of existing annotations is known') 
def get_max_annotation_id(context):
    # slightly dicey: knowing that annotations id increments, capture the current max id
    # and use it to identify the newly added entries in later tests:
    context.maxid = max_annotation_id(context)

@when('the annotations csv is ingested with author set to "{authorid}"')
def call_ingest_on_annotations_table(context, authorid):
    ingest_csv('annotations', context.dummycsvname, authorid=str(authorid))


@then('the annotations table should have {number} more entries')
def count_new_entries(context, number):
    with session_context() as session:
        new = session.query(Annotation).filter(Annotation.id>context.maxid)
        logging.info(f"new.count is {new.count()}")
        assert new.count() == int(number)

@then('the author of all those entries should be "{authorid}"')  # "sjl"
def check_author_of_new_entries(context, authorid):
    for entry in annotations(context, since=context.maxid):
        assert entry.authorid == authorid

@then('manual and systemdown should have boolean values')
def check_boolean_columns(context):
    for entry in annotations(context, since=context.maxid):
        assert entry.manual in { True, False }
        assert entry.systemdown in { True, False }

import datetime
@then('starttime and endtime should have valid datetimes')
def check_datetime_columns(context):
    for entry in annotations(context, since=context.maxid):
        logging.info(f"starttime: {entry.starttime}, endtime: {entry.endtime}, type is {type(entry.starttime)}")
        assert type(entry.starttime) == datetime.datetime
        assert type(entry.endtime) == datetime.datetime

@then('the author table should have an entry "{authorid}"') # "sjl"
def check_backpropagation_to_author_table(context, authorid):
    with session_context() as session:
        q = session.query(Author).filter_by(id=authorid).all()
        logging.info(q)
        assert len(q) > 0

@then('the component entry for the most recent annotation should have {cname}') # "c0-0c1s14n3"
def check_component_aliases_resolved(context, cname):
    with session_context() as session:
        maxid = max_annotation_id(context)
        q = session.query(Annotation).filter_by(id=maxid).all()
        #logging.info(q[0].components)
        #logging.info(cname)
        assert q[0].components == f'{cname}'


# Scenario: ingesting some jobs

@when('the {table} csv is ingested')
def ingest_a_csv(context, table):
    ingest_csv(table, context.dummycsvname)

@then('the nodelist for job {jobid} should be {nodelist}') # 12345 should be ["c0-0c1s14n3", "nid00124", "nid00125"]
def check_job_nodelist(context, jobid, nodelist):
    with session_context('jobs') as session:
        q = session.query(Job).filter_by(JobID=jobid).all()
        assert q[0].NodeList == nodelist



