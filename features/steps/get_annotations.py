from behave import *
import subprocess
import json

# Scenario: Retrieve annotations for a single component

@given('the annotations database has at least one entry for the component of interest')
def check_ann_exists(context):
    context.anno_cursor.execute("select count(*) from annotations where components like '%\"c0-0c0s5\"%'")
    assert context.anno_cursor.fetchone()[0] > 0

@when('I enter the component name at the command line')
def search_component(context):
    response = subprocess.run(["./anno.py", "-c", "c0-0c0s5"], stdout=subprocess.PIPE)
    context.output = response.stdout.decode('UTF-8')

@then('the results should include at least one annotation that applies to that component')
def component_present(context):
    assert context.failed is False
    assert 'Components: ["c0-0c0s5"]' in context.output

@then('the results contain no annotations that do not apply to that component')
def other_components_not(context):
    annotations = context.output.split('----------')
    num_bad = 0
    # drop the empty string that will be the first item in the list, and check the text of each annotation for a line that matches our criteria
    for annotation in annotations[1:]:
        lines = annotation.splitlines()
        for line in lines:
            if line.startswith('Components') and '"c0-0c0s5"' not in line:
                num_bad+= 1

    assert num_bad == 0

# Scenario: Retrieve annotations that apply to a time after a given timestamp

@given('the annotations database has at least one entry with an end time later than the given timestamp')
def search_start(context):
    context.starttime = "2015-05-05 12:15:00"
    context.anno_cursor.execute("select count(*) from annotations where endtime > '{0}'".format(context.starttime)) 
    assert context.anno_cursor.fetchone()[0] > 0

@when('I enter a start time at the command line')
def search_start(context):
    response = subprocess.run(["./anno.py", "-s", context.starttime], stdout=subprocess.PIPE)
    context.output = response.stdout.decode('UTF-8')

@then('the results should include at least one annotation with an end time that is after the given timestamp and none that has an end time before the given timestamp')
def after_start(context):
    assert context.failed is False
    annotations = context.output.split('----------')
    num_good = 0
    num_bad = 0
    for annotation in annotations[1:]:
        lines = annotation.splitlines()
        for line in lines:
            if line.startswith('Time'):
                timerange = line[6:].partition(' to ')
                if timerange[2] <= context.starttime:
                    num_bad+=1
                else: num_good+=1

    assert num_good > 0
    assert num_bad == 0


# Scenario: Retrieve annotations that apply to a time before a given timestamp

@given('the annotations database has at least one entry with a start time before the given timestamp')
def search_start(context):
    context.endtime = "2016-09-06 17:00:00"
    context.anno_cursor.execute("select count(*) from annotations where starttime < '{0}'".format(context.endtime)) 
    assert context.anno_cursor.fetchone()[0] > 0

@when('I enter an end time at the command line')
def search_start(context):
    response = subprocess.run(["./anno.py", "-e", context.endtime], stdout=subprocess.PIPE)
    context.output = response.stdout.decode('UTF-8')

@then('the results should include at least one annotation with a start time that is before the given timestamp and none that have a start time after the given timestamp')
def after_start(context):
    assert context.failed is False
    annotations = context.output.split('----------')
    num_good = 0
    num_bad = 0
    for annotation in annotations[1:]:
        lines = annotation.splitlines()
        for line in lines:
            if line.startswith('Time'):
                timerange = line[6:].partition(' to ')
                if timerange[1] >= context.endtime:
                    num_bad+=1
                else: num_good+=1

    assert num_good > 0
    assert num_bad == 0


# Scenario: Retrieve annotations timestamped during a given time range

@given('the annotations database has at least one entry for the time range of interest')
def search_range(context):
    context.earlytime = "2016-09-06 11:00:00"
    context.latetime = "2016-09-06 18:00:00"
    context.anno_cursor.execute("select count(*) from annotations where starttime between '{0}' and '{1}' or endtime between '{0}' and '{1}'".format(context.earlytime, context.latetime)) 
    assert context.anno_cursor.fetchone()[0] > 0

@when('I enter a start time and an end time at the command line')
def step_impl(context):
    response = subprocess.run(["./anno.py", "-s", context.earlytime, "-e", context.latetime], stdout=subprocess.PIPE)
    context.output = response.stdout.decode('UTF-8')

@then('the results should include at least one annotation with a start time before the entered end time and an end time after the entered start time, and no annotations that have a start time after the entered end time or an end time before the entered start time')
def step_impl(context):
    assert context.failed is False
    annotations = context.output.split('----------')
    num_good = 0
    num_bad = 0
    for annotation in annotations[1:]:
        lines = annotation.splitlines()
        for line in lines:
            if line.startswith('Time'):
                timerange = line[6:].partition(' to ')
                # check if the annotation start time is after the query time range or the annotations end time is before the query time range (which is bad)
                if timerange[0] > context.latetime or timerange[2] < context.earlytime:
                    num_bad+=1
                else: num_good+=1

    assert num_good > 0
    assert num_bad == 0


# Scenario: Retrieve annotations that contain a given string

@given('the annotations database has at least one entry that contains the given string')
def search_string(context):
    context.string = 'node down'
    context.anno_cursor.execute("select count(*) from annotations where {1} like '%{0}%' or {2} like '%{0}%' or {3} like '%{0}%' or {4} like '%{0}%' or {5} like '%{0}%'".format(context.string, 'description', 'endstate', 'ldcatgroup', 'ldcategory', 'ldtag')) 
    assert context.anno_cursor.fetchone()[0] > 0

@when('I enter a request for a type with the string at the command line')
def step_impl(context):
    response = subprocess.run(["./anno.py", "-t", context.string], stdout=subprocess.PIPE)
    context.output = response.stdout.decode('UTF-8')

@then('the results should include at least one entry that contains the string and no entries that do not contain the string')
def step_impl(context):
    assert context.failed is False
    annotations = context.output.split('----------')
    num_good = 0
    num_bad = 0
    for annotation in annotations[1:]:
        if context.string in annotation:
            num_good+=1
        else:
            num_bad+=1

    assert num_good > 0
    assert num_bad == 0


# Scenario: Retrieve annotations for a job

@given('the annotations database has an entry for at least one node on which the job ran at the time the job ran')
def step_impl(context):
    context.jobid = "10767913" #single node job for testing
    context.job_cursor.execute("select * from jobs where jobid = {0}".format(context.jobid))
    jobresult = context.job_cursor.fetchone()
    print("jobresult: ", jobresult)
    context.jobnids = json.loads(jobresult[3])
    context.jobstart = jobresult[5]
    context.jobend = jobresult[6]
    context.anno_cursor.execute("select count(*) from annotations where starttime <= '{0}' and endtime >= '{1}' and components like '%\"{2}\"%'".format(context.jobend, context.jobstart, context.jobnids[0]))
    assert context.anno_cursor.fetchone()[0] > 0

@when('I enter the job id at the command line')
def step_impl(context):
    response = subprocess.run(["./anno.py", "-j", context.jobid], stdout=subprocess.PIPE)
    context.output = response.stdout.decode('UTF-8')

@then('the results should include at least one annotation that applies to the time during which the job ran and that involves a node on which the job ran, and no results that only apply outside the time range or to nodes on which the job did not run')
def step_impl(context):
    assert context.failed is False
    annotations = context.output.split('----------')
    num_good = 0
    num_bad = 0
    for annotation in annotations[1:]:
        lines = annotation.splitlines()
        for line in lines:
            if line.startswith("Components:"):
                componentlist = json.loads(line[12:])
                for component in componentlist:
                    if component in context.jobnids:
                        num_good+=1
                        break #stop checking once you get one match
                    else:
                        num_bad+=1
            if line.startswith('Time'):
                timerange = line[6:].partition(' to ')
                # check if the annotation start time is after the query time range or the annotations end time is before the query time range (which is bad)
                if timerange[0] > context.jobend or timerange[2] < context.jobstart:
                    num_bad+=1
                else: num_good+=1

    assert num_good > 0
    assert num_bad == 0



# Scenario: Retrieve annotations in tabular format

@given('the annotations database has at least one entry for a component of interest')
def check_ann_exists(context):
    context.anno_cursor.execute("select count(*) from annotations where components like '%\"c0-0c0s5\"%'") 
    assert context.anno_cursor.fetchone()[0] > 0

@when('I enter the component name at the command line and specify tabular output')
def search_component(context):
    response = subprocess.run(["./anno.py", "-c", "c0-0c0s5", "-f", "table"], stdout=subprocess.PIPE)
    context.output = response.stdout.decode('UTF-8')

@then('the results should include at least one annotation formatted as a table')
def output_tabular(context):
    assert context.failed is False
    assert '\t' in context.output


# Scenario: Retrieve annotations in json format

@given('the annotations database has at least one entry for another component of interest')
def check_ann_exists(context):
    context.anno_cursor.execute("select count(*) from annotations where components like '%\"c0-0c0s5\"%'") 
    assert context.anno_cursor.fetchone()[0] > 0

@when('I enter the component name at the command line and specify json output')
def search_component(context):
    response = subprocess.run(["./anno.py", "-c", "c0-0c0s5", "-f", "json"], stdout=subprocess.PIPE)
    context.output = response.stdout.decode('UTF-8')

@then('the results should include at least one annotation formatted as json')
def output_json(context):
    assert context.failed is False
    json_str = json.loads(context.output)
    assert json_str



# Scenario: Retrieve no more than a given number of records

@given('the annotations database has at least ten entries')
def step_impl(context):
    context.anno_cursor.execute("select count(*) from annotations") 
    assert context.anno_cursor.fetchone()[0] > 2

@when('I enter a request for all entries with a limit of 2')
def step_impl(context):
    response = subprocess.run(["./anno.py", "--limit", "2"], stdout=subprocess.PIPE)
    context.output = response.stdout.decode('UTF-8')

@then('the results should include exactly 2 entries')
def step_impl(context):
    assert context.failed is False
    annotations = context.output.split('----------')
    assert len(annotations[1:]) == 2



# Scenario: Retrieve annotations for components a given number of hops apart

@given('the annotations database has at least one entry for a component that is one hop from a particular component and one that is two hops from it')
def step_impl(context):
    context.component1 = 'c0-0'
    context.component2 = 'smw'
    context.component3 = 'c0-0c0s6'
    context.anno_cursor.execute("select count(*) from annotations where components like '%\"{0}\"%'".format(context.component2)) 
    assert context.anno_cursor.fetchone()[0] > 0 
    context.anno_cursor.execute("select count(*) from annotations where components like '%\"{0}\"%'".format(context.component3)) 
    assert context.anno_cursor.fetchone()[0] > 0

@when('I enter the particular component id at the command line and specify two hops')
def step_impl(context):
    response = subprocess.run(["./anno.py", "-c", context.component1, "-d", "2"], stdout=subprocess.PIPE)
    context.output = response.stdout.decode('UTF-8')

@then('the results should include at least one annotation that applies to the component one hop away and one for the component that is two hops away')
def step_impl(context):
    assert context.failed is False
    annotations = context.output.split('----------')
    num_component2 = num_component3 = 0
    # drop the empty string that will be the first item in the list, and check the text of each annotation for a line that matches our criteria
    for annotation in annotations[1:]:
        lines = annotation.splitlines()
        for line in lines:
            print(line)
            if line.startswith('Components') and "\"{0}\"".format(context.component2) in line:
                num_component2+= 1
            if line.startswith('Components') and "\"{0}\"".format(context.component3) in line:
                num_component3+=1

    assert num_component2 > 0
    assert num_component3 > 0


# Scenario: Retrieve jobs that correspond to a specific annotation

@given('the annotations database has at least one entry for a job that ran during the time of a specific annotation and on the same machine')
def step_impl(context):
    context.annoid = 'abc22716'
    authid = context.annoid[0:3]
    annoid = context.annoid[3:]
    context.anno_cursor.execute("select starttime, endtime, system from annotations where authorid='{0}' and id={1}".format(authid, annoid))
    context.starttime, context.endtime, context.system = context.anno_cursor.fetchone()
    context.job_cursor.execute("select count(*) from jobs where Start<='{0}' and End>='{1}' and system like '{2}'".format(context.endtime, context.starttime, context.system))
    assert context.job_cursor.fetchone()[0] > 0 

@when('I enter the identifier for an annotation at the command line and specify jobs')
def step_impl(context):
    response = subprocess.run(["./anno.py", "--jobs", context.annoid], stdout=subprocess.PIPE)
    context.output = response.stdout.decode('UTF-8')

@then('the results should include at least one job that was running on that machine during the time range of the annotation.')
def step_impl(context):
    assert context.failed is False
    jobs = context.output
    assert len(jobs)>0

# Scenario: Retrieve the first annotation that has a start time after a given datetime

@given('the annotations database has at least one entry with a start time after the given time')
def search_start(context):
    context.starttime = "2015-05-05 12:15:00"
    context.anno_cursor.execute("select count(*) from annotations where endtime > '{0}'".format(context.starttime)) 
    assert context.anno_cursor.fetchone()[0] > 0

@when('I enter a request for the next annotation and enter a datetime at the command line')
def search_start(context):
    response = subprocess.run(["./anno.py", "-n", context.starttime], stdout=subprocess.PIPE)
    context.output = response.stdout.decode('UTF-8')

@then('the results should include one entry that has a starttime after the given time')
def after_start(context):
    assert context.failed is False
    annotations = context.output.split('----------') #stopped here
    assert len(annotations) == 2
    lines = annotations[1].splitlines()
    for line in lines:
        if line.startswith('Time'):
            timerange = line[6:].partition(' to ')
            assert timerange[2] > context.starttime

