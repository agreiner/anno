Feature: ingesting annotations

    As an annotator
    I want guidance and templates for adding annotations
    And checks and smarts to clean up annotations as I add them
    So that getting annotations into the database is easy and reliable

    Background: databases have been nominated
        Given anno.conf contains the text:
            """
            anno_db_path='../sample-data/annotations-test.sqlite3'
            job_db_path='../sample-data/jobs-test.sqlite3'
            """
        #Given an annotations database has been nominated
        #And a jobs database has been nominated

    Scenario Outline: creating and using annotations databases
        If I don't have a database already, create one for me
        Given the annotations db exists <yes or no>
        And the jobs db exists <yes or no>
        When db connections are made
        Then both databases exist and have the correct schemas
        Examples:
            | label       | annotations db exists | jobs db exists |
            | both exist  | yes                   | yes            |
            | anns exists | yes                   | no             |
            | jobs exists | no                    | yes            |
            | none exist  | no                    | no             |

    Scenario Outline: providing csv templates for user to add annotations and jobs
        Given a <table> has been nominated
        And the dbs are connected
        When write_csv_template is called
        Then a csv file for that <table> is produced
        Examples:
            | table              |
            | annotations        |
            | metaannotations    |
            | authors            |
            | physicalcomponents |
            | alias              |

    Scenario: ingesting some annotations
        Given the following annotations in a csv file
            | authorid | starttime | endtime | startstate | endstate | description                | manual | logfiles | system | systemdown | LDcatgroup | LDcategory | LDtag | components       | balerpatternid |
            | sjl      | 12/1/18   | 12/1/18 |            |          | annotation about something | TRUE   |          | cori   | N          |            |            |       | "cori01, cori02" |                |
            |          | 12/3/18   |         |            |          | yet another annotation     | T      | log1.log | cori   |            |            |            |       | c1-0c1s1n1       |                |
            |          | 12/2/18   | 12/3/18 |            |          | another annotation         | Y      |          | cori   | Y          |            |            |       | nid00123         |                |
        And the dbs are connected
        And the maximum id of existing annotations is known 
        And the alias table has entries like
            | name        | aliases      |
            | c0-0c1s14n3 | ["nid00123", "compute99"] |
        When the annotations csv is ingested with author set to "sjl"
        Then the annotations table should have 3 more entries
        And the author of all those entries should be "sjl"
        And manual and systemdown should have boolean values
        And starttime and endtime should have valid datetimes
        And the author table should have an entry "sjl"
        And the component entry for the most recent annotation should have ["c0-0c1s14n3"]

    Scenario: ingesting some jobs
        Given the following jobs in a csv file
            | JobID | UID | JobName  | NodeList       | NNodes | Start               | End                 | System |
            | 12345 | 0   | testjob  | nid00[123-125] | 3      | 2016-09-02 12:05:47 | 2016-09-02 12:38:00 | cielo  |
            | 12346 | 0   | testjob2 | nid00123       | 1      | 2016-09-02 12:05:47 | 2016-09-02 12:38:00 | cielo  |
        And the alias table has entries like
            | name        | aliases      |
            | c0-0c1s14n3 | ["nid00123", "compute99"] |
        When the jobs csv is ingested
        Then the nodelist for job 12345 should be ["c0-0c1s14n3", "nid00124", "nid00125"]
