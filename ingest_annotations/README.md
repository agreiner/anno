# Support for ingesting annotations

This directory includes two scripts to support ingesting annotations into an
annotations database:

- **csv_template.py** writes the heading of a table into a csv file, which can
  then be imported into a spreadsheet for easier editing.

  Running `csv_template.py --help` will produce the following usage text:

  ```
  csv_template.py [-h] [-f] [table] [outfilename]
  writes a CSV file with the headings for the selected table name
  this file can be manually edited and then ingested with ingest_csv.py
  The default table is 'annotations' and the default filename is <tablename>.csv'
  -f will overwrite an existing output file, if there is one

  Available tables are:
     annotations   LDgroups         metaannotations    authors
     jobs          componenttypes   associationtypes   linktypes
     architectures rtr              physicalcomponents link   
     alias
  ```

- **ingest_csv.py** reads data from a CSV-format file into a table, checking and 
  cleaning table entries along the way. It currently supports the main 'annotations' 
  and 'jobs' tables - for other tables, using `load_generic.py` in the `table_support/` 
  directory is recommended. 

  The tool sanitizes dates, boolean fields, component aliases and some missing data.
  You can specify "constant" values for authorid or system for all entries in a 
  `field=value` format 

  Running `ingest_csv.py --help` will produce the following usage text:

  ```
  ingest_csv.py [-h] [-t tablename] csvfile [field=value ...]
  reads a CSV file and ingests into the selected table
  The default table is 'annotations', which accepts global values
  for the fields authorid= and system=
  Currently supported tables are 'annotations' and 'jobs'. For other
  tables please use ../table_support/load_generic.py
  ```


This directory also has a `db_support.py` module, which provides the functionality
used in these tools.

  

# Copyright Notice

'Anno' Copyright (c) 2018, The Regents of the University of California, through Lawrence Berkeley National Laboratory, and National Technology & Engineering Solutions of Sandia, LLC, for the operation of Sandia National Laboratory (subject to receipt of any required approvals from the U.S. Dept. of Energy). All rights reserved.

If you have questions about your rights to use or distribute this software, please contact Berkeley Lab's Intellectual Property Office at IPO@lbl.gov.

NOTICE. This Software was developed under funding from the U.S. Department of Energy and the U.S. Government consequently retains certain rights. As such, the U.S. Government has been granted for itself and others acting on its behalf a paid-up, nonexclusive, irrevocable, worldwide license in the Software to reproduce, distribute copies to the public, prepare derivative works, and perform publicly and display publicly, and to permit other to do so.