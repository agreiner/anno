#!/usr/bin/env python3

from db_support import ingest_csv
import logging

import sys, getopt
if __name__ == '__main__':
    usage = f"{sys.argv[0]} [-h] [-t tablename] csvfile [field=value ...]\n"
    usage += "reads a CSV file and ingests into the selected table\n"
    usage += "The default table is 'annotations', which accepts global values\n"
    usage += "for the fields authorid= and system=\n"
    usage2  = "Currently supported tables are 'annotations' and 'jobs'. For other\n"
    usage2 += "tables please use ../table_support/load_generic.py\n"
    usage += usage2

    opts, args = getopt.getopt(sys.argv[1:], "hvt:", ["help", "verbose", "table="])
    tablename = 'annotations'
    fields = {}
    for o, a in opts:
        if o == "-h":
            print(usage)
            sys.exit()
        elif o in ["-v", "--verbose"]:
            logging.getLogger().setLevel(logging.DEBUG)
        elif o in  ["-t", "--table"]:
            tablename = a
        else:
            print(usage)
            sys.exit(1)

    infilename = ''
    if len(args) == 0:
        print(usage)
        sys.exit()
    infilename = args[0]
    for a in args[1:]:
        f,sep,v = a.partition('=')
        if not sep:
            print(usage)
            sys.exit(1)
        fields[f] = v

    keys = {}
    if tablename == 'annotations':
        keys = {'authorid', 'system'}
    elif tablename == 'jobs':
        keys = {'system'}
    else:
        print(usage2)
        sys.exit(1)

    ingest_csv(tablename, infilename, **{k:fields[k] for k in keys if k in fields})
