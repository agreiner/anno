#!/usr/bin/env python3

from db_support import write_csv_template

import sys, getopt
if __name__ == '__main__':
    usage = f"{sys.argv[0]} [-h] [-f] [table] [outfilename]\n"
    usage += "writes a CSV file with the headings for the selected table name\n"
    usage += "this file can be manually edited and then ingested with ingest_csv.py\n"
    usage += "The default table is 'annotations' and the default filename is <tablename>.csv'\n"
    usage += "-f will overwrite an existing output file, if there is one\n"
    usage += "\nAvailable tables are:\n"
    usage += "   annotations   LDgroups         metaannotations    authors\n"
    usage += "   jobs          componenttypes   associationtypes   linktypes\n"
    usage += "   architectures rtr              physicalcomponents link\n"
    usage += "   alias\n"

    opts, args = getopt.getopt(sys.argv[1:], "hf", ["help","force"])
    force = False
    for o, a in opts:
        if o in ["-h", "--help"]:
            print(usage)
            sys.exit()
        elif o in ["-f", "--force"]:
            force = True
        else:
            print(usage)
            sys.exit(1)

    tablename = 'annotations'
    outfilename = f'{tablename}.csv'
    if len(args) > 0:
        tablename = args[0]
        outfilename = f'{tablename}.csv'
    if len(args) > 1:
        outfilename = args[1]

    write_csv_template(tablename, outfilename)

