#!/usr/bin/env python3

# type hints require python 3.5+
import sys
if sys.version_info < (3,5):
    raise Exception("Requires python 3.5+")

import logging

import sqlalchemy as sql
from sqlalchemy.ext.declarative import declarative_base

import pandas as pd
import dateutil.parser

from typing import Dict, List, Optional, Tuple

# -- annotations db --
AnnoBase = declarative_base(metadata=sql.MetaData())

class Alias(AnnoBase):
    __tablename__ = 'alias'
    name = sql.Column(sql.String, primary_key = True)
    aliases = sql.Column(sql.String)

    _as_dict = None
    @classmethod
    def as_dict(cls, session):
        if cls._as_dict is None:
            df = pd.read_sql_query('select name, aliases from alias', 
                                   session.connection())
            cls._as_dict = {}
            for t in df.itertuples():
                for alias in json.loads(t.aliases):
                    cls._as_dict[alias] = t.name
        return cls._as_dict

class LDgroup(AnnoBase):
    __tablename__ = "LDgroups"
    id = sql.Column(sql.String(2), primary_key=True)
    groupname = sql.Column(sql.String, nullable=False, primary_key=True)
    # relationships:
    annotations = sql.orm.relationship("Annotation", back_populates="ldgroup")

class Author(AnnoBase):
    __tablename__ = "authors"
    id = sql.Column(sql.String(3), nullable=False, primary_key=True)
    name = sql.Column(sql.String)
    institution = sql.Column(sql.String)
    # relationships
    annotations = sql.orm.relationship("Annotation", back_populates="author")

class Annotation(AnnoBase):
    __tablename__ = "annotations"
    id = sql.Column(sql.Integer, nullable=False, primary_key=True)
    authorid = sql.Column(sql.String, sql.ForeignKey('authors.id'), 
                          nullable=False, primary_key=True)
    starttime = sql.Column(sql.DateTime, nullable=False, index=True)
    endtime = sql.Column(sql.DateTime, nullable=False, index=True)
    startstate = sql.Column(sql.String)
    endstate = sql.Column(sql.String)
    description = sql.Column(sql.String, nullable=False)
    manual = sql.Column(sql.Boolean)
    logfiles = sql.Column(sql.String)
    system = sql.Column(sql.String)
    systemdown = sql.Column(sql.Boolean)
    LDcatgroup = sql.Column(sql.String, sql.ForeignKey("LDgroups.id"))
    LDcategory = sql.Column(sql.String)
    LDtag = sql.Column(sql.String)
    components = sql.Column(sql.String)
    balerpatternid = sql.Column(sql.Integer)
    # relationships:
    ldgroup = sql.orm.relationship("LDgroup", back_populates="annotations")
    author = sql.orm.relationship("Author", back_populates="annotations")

    @classmethod
    def ingest_from_csv(cls, csvfilepath: str, session, 
                        authorid:Optional[str]=None, system:Optional[str]=None, **kwargs):
        df = pd.read_csv(csvfilepath, dtype=str)
        # drop extraneous columns
        todrop = set(df) - set([col.name for col in cls.__table__.columns])
        df.drop(list(todrop), axis=1)
        # check that we have min required columns:
        required = {'authorid','starttime','endtime','description','system'}
        # optionally set whole-column values:
        if authorid is not None:
            df['authorid'] = authorid
        if system is not None:
            df['system'] = system
        required_but_missing = required - set(df)
        if len(required_but_missing)>0:
            raise Exception(f"missing values for some required columns: {required_but_missing}")
        # clean up dates
        df['endtime'].where(df['endtime'].notnull(), df['starttime'], inplace=True)
        df['starttime'] = df['starttime'].apply(dateutil.parser.parse)
        df['endtime'] = df['endtime'].apply(dateutil.parser.parse)
        # clean up booleans:
        trues = {'TRUE','T','Y','YES','1'}
        means_true = lambda x: True if str(x).upper() in trues else False
        df['manual'] = df['manual'].apply(means_true)
        df['systemdown'] = df['systemdown'].apply(means_true)
        # replace components with canonical ones
        canonicalize = lambda x: canonicalize_components(session, x)
        df['components'] = df['components'].apply(canonicalize)
        nextid = (session.query(sql.sql.expression.func.max(cls.id)).scalar() or -1)+1
        df['id']=df.index.to_series().apply(lambda x: x+nextid) 
        # make sure we have sufficient valid data:
        for c in required:
            if df[c].isnull().values.any():
                raise Exception(f"missing values for required column {c}")
        # make sure the authorids and LDcatgroups all exist
        for authorid in df['authorid'].dropna().unique():
            if not session.query(Author).filter_by(id=authorid).count():
                session.add(Author(id=authorid))
        for ldcatgroup in df['LDcatgroup'].dropna().unique():
            if not session.query(LDgroup).filter_by(id=ldcatgroup).count():
                session.add(LDgroup(id=ldcatgroup, groupname='FIXME'))
        # save to db:
        session.bulk_insert_mappings(cls, df.to_dict(orient='records'))

# -- jobs --
JobsBase = declarative_base(metadata=sql.MetaData())

class Job(JobsBase):
    __tablename__ = "jobs"
    JobID = sql.Column(sql.String, nullable=False, primary_key=True)
    UID = sql.Column(sql.Integer)
    JobName = sql.Column(sql.String)
    NodeList = sql.Column(sql.String)
    NNodes = sql.Column(sql.Integer)
    Start = sql.Column(sql.DateTime, index=True)
    End = sql.Column(sql.DateTime, index=True)
    System = sql.Column(sql.String)

    @classmethod
    def ingest_from_csv(cls, csvfilepath: str, session,
                        System:Optional[str]=None, **kwargs):
        df = pd.read_csv(csvfilepath, dtype=str)
        # drop extraneous columns
        todrop = set(df) - set([col.name for col in cls.__table__.columns])
        df.drop(list(todrop), axis=1)
        # check that we have min required columns:
        required = {'JobID','Start','End','System'}
        # optionally set whole-column values:
        if System is not None:
            df['System'] = System
        required_but_missing = required - set(df)
        if len(required_but_missing)>0:
            raise Exception(f"missing values for some required columns: {required_but_missing}")
        # clean up dates
        df['Start'] = df['Start'].apply(dateutil.parser.parse)
        df['End'] = df['End'].apply(dateutil.parser.parse)
        # replace components with canonical ones
        # need a separate session to the annotations DB:
        with session_context() as anno_session: 
            canonicalize = lambda x: canonicalize_components(anno_session, x)
            df['NodeList'] = df['NodeList'].apply(canonicalize)
        # make sure we have sufficient valid data:
        for c in required:
            if df[c].isnull().values.any():
                raise Exception(f"missing values for required column {c}")
        # save to db:
        session.bulk_insert_mappings(cls, df.to_dict(orient='records'))


# -- db operations --

def connect(base, db_path):
    db_engine = sql.create_engine(f'sqlite:///{db_path}', echo=False)
    base.metadata.create_all(db_engine)
    return sql.orm.sessionmaker(bind=db_engine)

import os
import subprocess
def create_anno_db(anno_db_path: str, force:bool = False) -> None:
    """ annotations db schema is greater than what we use here, so if it doesn't
        already exist we need to create it correctly by running the appropriate sqlite script
    """
    if os.path.isfile(anno_db_path) and not force:
        raise Exception(f"cowardly refusing to stomp on existing file {anno_db_path}")

    anno_dir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
    schema_script_path = os.path.join(anno_dir, 'table_support', 'createAnnotationsDB.txt')
    schema_script = open(schema_script_path)
    cmd = f'sqlite3 {anno_db_path}'.split()
    subp = subprocess.run(cmd, stdin=schema_script, stdout=subprocess.DEVNULL)
    subp.check_returncode()

    # also populate it with fixed/initial data:
    populate_script_path = os.path.join(anno_dir, 'table_support', 'populateTables.txt')
    populate_script = open(populate_script_path)
    subp = subprocess.run(cmd, stdin=populate_script, stdout=subprocess.DEVNULL)
    subp.check_returncode()

AnnoSessionManager = None
def connect_anno_db(db_path):
    if not os.path.isfile(db_path):
        create_anno_db(db_path)

    global AnnoSessionManager
    AnnoSessionManager = connect(AnnoBase, db_path)
    return AnnoSessionManager

def create_jobs_db(jobs_db_path: str) -> None:
    """ mirror create_anno_db functionality, mostly in support of testing """
    jobs_session = connect_jobs(jobs_db_path)
    jobs_session.commit()
    jobs_session.close()

JobsSessionManager = None
def connect_jobs_db(db_path):
    global JobsSessionManager
    JobsSessionManager = connect(JobsBase, db_path)
    return JobsSessionManager

def connect_all():
    """ connect to both databases """
    anno_db_path, jobs_db_path = read_paths()
    global AnnoSessionManager
    global JobsSessionManager
    AnnoSessionManager = connect_anno_db(anno_db_path)
    JobsSessionManager = connect_jobs_db(jobs_db_path)

from contextlib import contextmanager
@contextmanager
def session_context(db:str='annotations'):
    """Provide a transactional scope around a series of operations."""
    if db == 'annotations':
        if not AnnoSessionManager:
            connect_all()
        session = AnnoSessionManager(autoflush=False)
    elif db == 'jobs':
        if not JobsSessionManager:
            connect_all()
        session = JobsSessionManager(autoflush=False)
    else:
        raise ValueError(f"arg must be 'annotations' or 'jobs', not {db}")
    try:
        yield session
        session.commit()
    except:
        session.rollback()
        raise
    finally:
        session.close()


# -- utilities --

import string
import re
def expand_nodelist(nlist: str) -> List[str]:
    """ translate a nodelist like 'nid[02516-02575,02580-02635,02836]' into a
        list of explicitly-named nodes, eg 'nid02516 nid02517 ...'
    """
    nodes = []
    if nlist.count('[') != nlist.count(']'):
        raise Exception(f"Incomplete nodelist: {nlist}")
    prefix, sep0, nl = nlist.partition('[')
    if sep0:
        for component in nl.rstrip(']').split(','):
            first,sep1,last = component.partition('-')
            width=f'0{len(first):d}d'
            if sep1:
                nodes += [ f'{prefix:s}{i:{width:s}}' 
                           for i in range(int(first),int(last)+1) ] 
            else:
                nodes += [ f'{prefix:s}{first:{width:s}}' ]
    else:
        nodes += [ prefix ]
    return nodes

def parse_component_string(components:str) -> List[str]:
    """ given a not-cleanly-formatted string describing a set 
        of components, return a corresponding predictably-formatted
        list of those components
    """
    clist = []
    # remove quotation marks
    components = re.sub('[\'"]', '', components)
    # find compact nidlists to expand (slurm)
    re_nidlist = re.compile('(?P<nidlist>nid0*\[[0-9,-]+\])')
    logging.info(f"checking components {components}")
    for nidlist in re_nidlist.findall(components):
        logging.info(f"expanding nidlist {nidlist}")
        clist += expand_nodelist(nidlist)
    remaining = ' '.join(re_nidlist.split(components)[0::2]).strip()
    logging.info(f"remaining is '{remaining}'")
    logging.info(f"clist is {clist}")
    # split what remains on words (space, comma etc):
    clist += [ x for x in re.split('[ ,]+', remaining) if x ]
    logging.info(f"clist is {clist}")
    return clist

import json
def canonicalize_components(session, components: str) -> str:
    """ given a list of components, or a nidlist in compact format, expand
        each component and check for a known canonical name to replace it with
    """
    logging.info(f"canonicalizing {components}")
    try:
        clist = json.loads(components)
    except ValueError:
        clist = parse_component_string(components)
    # look for known aliases in expanded component list:
    aliases = Alias.as_dict(session)
    logging.info(f"aliases has {aliases}")
    clist2 = [ aliases.get(name, name) for name in clist ]
    logging.info(f"canonicalize returns {json.dumps(clist2)}")
    return json.dumps(clist2)

def read_paths() -> Tuple[str, str]:
    """ use anno.conf or defaults to choose path for anno and jobs dbs """
    # defaults:
    anno_db_path = 'annotations.sqlite3'
    jobs_db_path = 'jobs.sqlite3'

    try:
        config=open('anno.conf', 'r')
        for line in config:
            m = re.match('anno_db_path=\'([^=]*)\'', line)
            if m:
                anno_db_path = m.group(1)
            m = re.match('job_db_path=\'([^=]*)\'', line)
            if m:
                jobs_db_path = m.group(1)
    except IOError:
        print('No anno.conf file found. Using defaults.')

    return anno_db_path, jobs_db_path

def write_csv_template(tablename: str, csvname='template.csv') -> None:
    qtext = f'SELECT * from {tablename} limit 0'
    if tablename == 'jobs':
        db = 'jobs'
    else:
        db = 'annotations'
    with session_context(db) as session:
        df = pd.read_sql_query(qtext, session.connection())
        if tablename in ('annotations', 'metaannotations', 'physicalcomponents', 'rtr', 'link'):
            # leave autoincrementing ids out
            df = df.drop('id',axis=1)
        df.to_csv(csvname, index=False)

#def ingest_csv(tablename: str, csvpath: str, authorid:Optional[str]=None, system:Optional[str]=None) -> None:
def ingest_csv(tablename: str, csvpath: str, **kwargs) ->None:
    df = pd.read_csv(csvpath, dtype=str)
    if tablename == 'annotations':
        with session_context() as session:
            #Annotation.ingest_from_csv(csvpath, session, authorid, system)
            Annotation.ingest_from_csv(csvpath, session, **kwargs)
    elif tablename == 'jobs':
        with session_context('jobs') as session:
            Job.ingest_from_csv(csvpath, session, **kwargs)
    else:
        raise NotImplementedError
