#!/usr/bin/perl

# CREATE TABLE 'link' (
#       'id' integer,
#       'type' text REFERENCES 'linktypes'('id')
#       'E0' text,
#       'E1' text,
#       'R0' text,
#       'R1' text,
#    );

# GEMINI
# c0-0c0s0g0l00[(0,0,0)] Z+ -> c0-0c0s1g0l32[(0,0,1)]   LinkType: backplane
# c0-0c0s0g0l01[(0,0,0)] Z+ -> c0-0c0s1g0l21[(0,0,1)]   LinkType: backplane
# c0-0c0s0g0l02[(0,0,0)] X+ -> c1-0c0s0g0l02[(1,0,0)]   LinkType: cable11x
# c0-0c0s0g0l03[(0,0,0)] X+ -> c1-0c0s0g0l03[(1,0,0)]   LinkType: cable11x
# c0-0c0s0g0l04[(0,0,0)] X- -> c2-0c0s0g0l41[(15,0,0)]   LinkType: cable18x
#ARIES
#c0-5c0s0a0l00(30:0:0) green -> c0-5c0s6a0l10(30:0:6)
#c0-5c0s0a0l01(30:0:0) blue -> unused
#c0-5c0s0a0l02(30:0:0) blue -> unused

=pod

=head1 NAME

interconnectExtractor.pl

=head1 SYNOPSIS

rtr --interconnect > rtr.out
cat rtr.out | ./interconnectExtractor.pl > links.out
./load_generic.py links.out <database> links ','

=head1 DESCRIPTION

For Cray systems, extracts link endpoint information from
the interconnect information output by I<rtr --interconnect>
and converts it into the format necessary to load into the
I<links> table.

=head1 INSTRUCTIONS

=over

=item 1. Get the interconnect information via rtr --interconnect > rtr.out

=item 2. cat rtr.out | ./interconnectExtractor.pl > links.out

=item 3. Load I<links.out> into the I<links> table of your database with delimeter ','.

=back

=head1 NOTES

=over

=item * This is for Cray systems only, both Gemini and Aries.

=item * Router information is extracted using either I<hostInterconnectExtractor.pl>
or I<systemMapExtractor.pl>

=item * The scripts are intended to be a guide for building the material
to be loaded into the tables. They are not production hardened for
arbitrary systems.


=back

=head1 SEE ALSO
hostInterconnectExtractor.pl, load_generic.py, systemMapExtractor.pl, README_table_support.txt

=cut



use Scalar::Util qw(looks_like_number);

$linkid = 0;

print "id,type,E0,E1,R0,R1\n";

while(<>){
    chomp;
    $line = $_;

    if ($line =~ /\->/){
	@vals = split(/\s+/,$line);
	#well known
	$E0 = -1;
	$E1 = -1;
	$R0 = -1;
	$R1 = -1;

	if (($vals[0] =~ /(.*)\[/) || ($vals[0] =~ /(.*\d)\(/)){
	    $E0 = $1;
	    if ($E0 =~ /(.*)l/){
		$R0 = $1;
	    }
	}

	$type = -1;
	if ($vals[1] eq 'X+'){
	    $type = "XP0";
	} elsif ($vals[1] eq 'X-'){
	    $type = "XM0";
	} elsif ($vals[1] eq 'Y+'){
	    $type = "YP0";
	} elsif ($vals[1] eq 'Y-'){
	    $type = "YM0";
	} elsif ($vals[1] eq 'Z+'){
	    $type = "ZP0";
	} elsif ($vals[1] eq 'Z-'){
	    $type = "ZM0";
	} elsif ($vals[1] eq 'blue'){
	    $type = "BLU";
	} elsif ($vals[1] eq 'black'){
	    $type = "BLK";
	} elsif ($vals[1] eq 'green'){
	    $type = "GRE";
	} elsif ($vals[1] eq 'ptile'){
	    $type = "PTL";
	} elsif ($vals[1] eq 'host'){
	    $type = "HST";
	} else {
	    $type = "UNK";
	}


	if (($vals[3] =~ /(.*)\[/) || ($vals[3] =~ /(.*\d)\(/)){
	    $E1 = $1;
	    if ($E1 =~ /(.*)l/){
		$R1 = $1;
	    }
	} elsif ($vals[3] =~ /unused/){
	    #type is still correct for this tile
	    $E1 = 'unused';
	    $R1 = 'unused';
	} elsif ($vals[3] =~ /processor/){
	    #type is still correct for this tile
	    $E1 = 'processor';
	    $R1 = 'processor';
	}


	print $linkid++ . "," . $type . ",$E0,$E1,$R0,$R1\n";
    }
}
