-- ------------- BEGIN JOBS -------------------------#

CREATE TABLE jobs (
    JobID text, 
    UID int, 
    JobName text, 
    NodeList text, 
    NNodes int, 
    Start text, 
    End text, 
    system text);
CREATE INDEX starttime on jobs (start asc) ;
CREATE INDEX endtime on jobs (end asc) ;

-- ------------- END JOBS -------------------------#
