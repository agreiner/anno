#!/usr/bin/perl

# WANT:
#starttime,endtime,authorid,manual,LDcatgroup,components,system,description,startstate,endstate
#2015-03-03 00:00:01,23:59:59,sys,1,NO,unknown,system_name,"hardware change","bad hardware","new hardware"
#2015-03-05 00:00:01,23:59:59,sys,1,NE,unknown,system_name,"link tune in response to aries errors","aries errors","link tune"
#
# HAVE
#p0-20150211t172524
#p0-20150212t152812

=pod

=head1 NAME

buildP0Annotation.pl

=head1 SYNOPSIS

ls /var/opt/cray/log/p0* | ./buildP0Annotation.pl > p0anno.out
./load_generic.py ./p0anno.out <database> annotations ','

=head1 DESCRIPTION

For Cray systems, takes the occurrences of p0-YYYYMMDD directories, which
indicate system reboots, and turns them into loadable annotations whose
time range spans the times between each pair of reboots.

=head1 INSTRUCTIONS

=over

=item 1. Edit the indicated parameters in the file for your system name and author
to whom the annotation will be attributed.

=item 2. Get the dates of the p0 directories and pipe that into I<buildP0Annotation.pl>.
The output is the annotations.

=item 3. Load the output into the I<annotations> table of the database
using I<load_generic.py>.

=back

=head1 NOTES

=over

=item * For Cray systems only.

=item * The scripts are intended to be a guide for building the material
to be loaded into the tables. They are not production hardened for
arbitrary systems.

=back

=head1 SEE ALSO

README_table_support.txt

=cut


$system="system_name"; # replace with your system name
$author="sys"; # replace with the author to which this annotation will be attributed
$prevdatestr = "";
while (<>){
    chomp;
    $line = $_;
    if ($line =~ /p0-(\d\d\d\d)(\d\d)(\d\d)t(\d\d)(\d\d)(\d\d)/){
	$year = $1;
	$month = $2;
	$day = $3;
	$hour = $4;
	$minute = $5;
	$sec = $6;

	$datestr = "$year-$month-$day $hour:$minute:$sec";
	print "$datestr,$datestr,$author,1,NO,unknown,$sys,\"reboot (p0)\",$prevdatestr,$datestr\n";
	$prevdatestr = $datestr;
    }
}
