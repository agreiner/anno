#!/usr/bin/perl

# 2 files come out of this -- 1 for the alias and 1 for the rtr table

# Gemini:
# NID    NIC-Addr  Node          Gemini         X  Y  Z
# ----   --------  ------------  ------------   -- -- --
# 0        0       c0-0c0s0n0    c0-0c0s0g0     0  0  0
#
# Aries:
# NID    NIC-Addr   Node           Aries            Blue  Black  Green
# ----   --------   ----------     ------------     ----  -----  -----
# 0         0       c0-0c0s0n0     c0-0c0s0a0        0      0      0
# 1         1       c0-0c0s0n1     c0-0c0s0a0        0      0      0

=pod

=head1 NAME

systemMapExtractor.pl

=head1 SYNOPSIS

rtr -Im > systemmap.out
cat systemmap.out | ./systemMapExtractor.pl # This outputs alias.out and rtrtable.out
./load_generic.py rtrtable.out <database> rtr ';'
./load_generic.py alias.out <database> alias ','

=head1 DESCRIPTION

For Cray systems, extracts router and cname-to-nid association from the
system map information output by I<rtr -Im> and converts it into the format
necessary to load into the I<rtr> and I<alias> tables.

=head1 INSTRUCTIONS

=over

=item 1. Get the system map via rtr -Im > systemmap.out

=item 2. cat systemmap.out | ./systemMapExtractor.pl. The outputs
are alias.out and rtrtable.out.

=item 3. Add any other aliases to alias.out. At a minimum the
smw will need to be added.

=item 4. Load I<rtrtable.out> into the I<rtr> table of your database with delimeter ';'.

=item 5. Load I<alias.out> into the I<alias> table of your database with delimeter ','.

=back

=head1 NOTES

=over

=item * This is for Cray systems only, both Gemini and Aries.

=item * The alias information extracted is only the cname-to-nid. Any
other alias information, such as lnet nodes, etc, has to be added to
the alias file manually. In particular, the smw should be added.

=item * The I<hostInterconnectExtractor.pl> is an alternate method to
get I<rtr> and I<alias> output from I</etc/hosts> and I<rtr --interconnect>.

=item * Link endpoint information is extracted using I<interconnectExtractor.pl>

=item * The scripts are intended to be a guide for building the material
to be loaded into the tables. They are not production hardened for
arbitrary systems.


=back

=head1 SEE ALSO

interconnectExtractor.pl, hostInterconnectExtractor.pl, load_generic.py, README_table_support.txt

=cut



use Scalar::Util qw(looks_like_number);

$ALIASOUT = "alias.out";
$RTROUT = "rtrtable.out";

open ($afh, ">", $ALIASOUT) || die "Cannot open alias output file '$ALIASOUT': $!";

sub printRTROUT{
    open ($rfh, ">", $RTROUT) || die "Cannot open alias output file '$RTROUT': $!";

    print $rfh "id;type;cname;NICS;blade;nettopo\n";
    @sortedkeys = sort { $rtrmap{$a}{uid} <=> $rtrmap{$b}{uid} } keys(%rtrmap);
    foreach  $comp ( @sortedkeys){
	print $rfh "$rtrmap{$comp}{uid};";
	print $rfh "$rtrmap{$comp}{type};";
	print $rfh "$rtrmap{$comp}{name};";
	print $rfh "[";
	@arr = @{$rtrmap{$comp}{NICS}};
	$numnics = scalar(@arr);
	for ($i = 0; $i < scalar(@arr); $i++){
	    print $rfh "\"" . $arr[$i] . "\"";
	    if ($i != (scalar(@arr)-1)){
		print $rfh ",";
	    }
	}
	print $rfh "];";
	print $rfh "$rtrmap{$comp}{blade};";
	print $rfh "$rtrmap{$comp}{nettopo}";
	print $rfh "\n";
    }
    close $rfh;
}

print $afh "name,aliases\n";

%fields;
%rtrmap;

$rtrid = 0;

while(<>){
    chomp;
    $line = $_;
    if ($line =~ /NID/){
	@headers = split(/\s+/,$line);
	for ($i = 0; $i < scalar(@headers); $i++){
	    $fields{$headers[$i]} = $i;
	    # know that NID and Node are in there.
	}
    } else {
	@vals = split(/\s+/,$line);
	if (looks_like_number($vals[$NID])){

	    # write the nid to cname mapping to the alias file

	    $nidnum = $vals[$fields{NID}];
	    #change this to correctly formatted nid
	    $lnid = sprintf("nid%05d", $nidnum);
	    $lcname = $vals[$fields{Node}];
	    if ($lcname =~ /(.*)n(\d+)/){
		$reln = $2;
	    } else {
		die "Bad format '$lcname'\n";
	    }

# only print out the canonical one. all others go in as json.
# from this data source, its only nids. others will have to
# be added in by hand
#	    print $afh "$lnid,$lcname\n";
#	    print $afh "$lcname,$lnid\n";
	    print $afh "$lcname\t[\"$lnid\"]\n";

	    # get the rtr data
	    if (exists $fields{Gemini}){
		$rtr = $vals[$fields{Gemini}];
		$blade = -1;
		if ($rtr =~ /(.*)g\d+/){
		    $blade = $1;
		}
	    } elsif (exists $fields{Aries}){
		$rtr = $vals[$fields{Aries}];
		$blade = -1;
		if ($rtr =~ /(.*)a\d+/){
		    $blade = $1;
		}
	    }
	    ($blade != -1) || die "Cannot get blade from '$rtr'\n";

	    $currNIC = $rtr . "n" . $reln;
	    if (exists $rtrmap{$rtr}){
		push(@{$rtrmap{$rtr}{NICS}},$currNIC);
	    } else {
		if (exists $fields{X}){
		    $nettopo = $vals[$fields{X}] . ":" . $vals[$fields{Y}] . ":" . $vals[$fields{Z}];
		} elsif (exists $fields{Blue}){
		    $nettopo = $vals[$fields{Blue}] . ":" . $vals[$fields{Black}] . ":" . $vals[$fields{Green}];
		} else {
		    die "Cannot get nettopo\n";
		}

		$rtrmap{$rtr} = {
		    uid => $rtrid++,
		    name => $rtr,
		    type => "RTR",
		    NICS => [],
		    blade => $blade,
		    nettopo => $nettopo,
		};
		push(@{$rtrmap{$rtr}{NICS}},$currNIC);
	    } # exists
	} # number (NIDline)
    }
} # while

close $afh;

printRTROUT();
