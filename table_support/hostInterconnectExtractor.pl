#!/usr/bin/perl

# this is peer to systemMapExtractor.pl -- without the systemmap, we have to back nettopo out of the interconnect file
# 2 files go into this -- /etc/hosts and the output of rtr --interconnect

=pod

=head1 NAME

hostInterconnectExtractor.pl

=head1 SYNOPSIS

rtr --interconnect > rtr.out
./hostInterconnectExtractor.pl --rtrfile rtr.out --hostfile /etc/hosts # This outputs alias.out and rtrtable.out
./load_generic.py rtrtable.out <database> rtr ';'
./load_generic.py alias.out <database> alias ','

=head1 DESCRIPTION

For Cray systems, extracts router and cname-to-nid association from the
hosts file and router interconnect information output by I<rtr --interconnect>
and converts it into the format
necessary to load into the I<rtr> and I<alias> tables.

=head1 INSTRUCTIONS

=over

=item 1. Get the router connectivity information via rtr --interconnect > rtr.out

=item 2. ./hostInterconnectExtractor.pl --rtrfile rtr.out --hostfile /etc/hosts
The outputs are alias.out and rtrtable.out.

=item 3. Add any other aliases to alias.out. At a minimum the
smw will need to be added.

=item 4. Load I<rtrtable.out> into the I<rtr> table of your database with delimeter ';'.

=item 5. Load I<alias.out> into the I<alias> table of your database with delimeter ','.


=back

=head1 NOTES

=over

=item * This is for Cray systems only, both Gemini and Aries.

=item * The alias information extracted is only the cname-to-nid. Any
other alias information, such as lnet nodes, etc, has to be added to
the alias file manually. In particular, the smw should be added.

=item * The I<systemMapExtractor.pl> is an alternate method to
get I<rtr> and I<alias> output from  I<rtr -Im>.

=item * Link endpoint information is extracted using I<interconnectExtractor.pl>

=item * The scripts are intended to be a guide for building the material
to be loaded into the tables. They are not production hardened for
arbitrary systems.


=back

=head1 SEE ALSO
interconnectExtractor.pl load_generic.py, systemMapExtractor.pl, README_table_support.txt.

=cut


#use strict;
#use warnings;
use Getopt::Long;
use Pod::Usage;
use IO::Handle;
use Scalar::Util qw(looks_like_number);

my $ALIASOUT = "alias.out";
my $RTROUT = "rtrtable.out";

my $rtrfile = "";
my $hostfile = "";
my $help;

GetOptions(
    "help" => \$help,
    "rtrfile=s" => \$rtrfile,
    "hostfile=s" => \$hostfile
) or die ("CLI argument error");

if ($help) {
    pod2usage( -verbose => 2);
}

if (($rtrfile eq "") || ($hostfile eq "")){
    pod2usage( -verbose => 2);
}

my %rtrmap;

sub printRTROUT{
    open (my $rfh, ">", $RTROUT) || die "Cannot open output file '$RTROUT': $!";

    print $rfh "id,type,cname,NICS,blade,nettopo\n";
    my @sortedkeys = sort { $rtrmap{$a}{uid} <=> $rtrmap{$b}{uid} } keys(%rtrmap);
    foreach my $comp ( @sortedkeys){
	print $rfh "$rtrmap{$comp}{uid},";
	print $rfh "$rtrmap{$comp}{type},";
	print $rfh "$rtrmap{$comp}{name},";
	print $rfh "\"";
	my @arr = @{$rtrmap{$comp}{NICS}};
	my $numnics = scalar(@arr);
	for (my $i = 0; $i < scalar(@arr); $i++){
	    print $rfh "$arr[$i]";
	    if ($i != (scalar(@arr)-1)){
		print $rfh ",";
	    }
	}
	print $rfh "\",";
	print $rfh "$rtrmap{$comp}{blade},";
	print $rfh "$rtrmap{$comp}{nettopo}";
	print $rfh "\n";
    }
    close $rfh;
}



open (my $hfh, "<", $hostfile) || die "Cannot open hostfile input file '$hostfile': $!";
open (my $afh, ">", $ALIASOUT) || die "Cannot open alias output file '$ALIASOUT': $!";


print $afh "name,aliases\n";

# get the aliases for the nid-cnames only for now
while(<$hfh>){
    chomp;
    my $line = $_;
    if ($line =~ /nid/){
	my @vals = split(/\s+/, $line);
	if ((scalar(@vals) >= 3) && ($vals[1] =~ /nid/) && ($vals[2] =~ /c\d+\-\d+c\d+s\d+n\d+/)) {
	    my $lnid = $vals[1];
	    my $lcname = $vals[2];
# SEE note in systemMapExtractor.pl - only nid will be an alias. use json format. add others by hand.
#	    print $afh "$lnid,$lcname\n";
#	    print $afh "$lcname,$lnid\n";
	    print $afh "$lcname\t[\"$lnid\"]\n";
	}
    }
}

close $hfh;
close $afh;


my $rtrid = 0;

open (my $rfh, "<", $rtrfile) || die "Cannot open interconnect input file '$rtrfile': $!";

while(<$rfh>){
    chomp;
    my $line = $_;
    if ($line =~ /^c\d+\-\d+c\d+s\d+/){
	# the first val wont be unused
	my @vals = split(/\s+/,$line);

	if ($vals[0] =~ /(.*)l.*\((.*)\)/){
	    # get the rtr data
	    my $rtr = $1;

	    if (!exists $rtrmap{$rtr}){
		my $nettopo = $2;
		$nettopo =~ s/,/:/g;

		my $blade = "";
		my $gmin = -1;
		my $gmax = -1;
		if ($rtr =~ /(.*)g(\d+)/){
		    $blade = $1;
		    if ($2 == 0){
			$gmin = 0;
			$gmax = 1;
		    } else {
			$gmin = 2;
			$gmax = 3;
		    }
		} elsif ($rtr =~ /(.*)a\d+/){
		    $blade = $1;
		    $gmin = 0;
		    $gmax = 3;
		} else {
		    die "Bad rtr '$rtr'";
		}

		$rtrmap{$rtr} = {
		    uid => $rtrid++,
		    name => $rtr,
		    type => "RTR",
		    NICS => [],
		    blade => $blade,
		    nettopo => $nettopo,
		};

		for (my $i = $gmin; $i <= $gmax ; $i++){
		    # NIC name will not be nid cname, but rather routername + nX
#		    my $nic = $blade . "n" . $i;
		    my $nic = $rtr . "n" . $i;
		    push(@{$rtrmap{$rtr}{NICS}},$nic);
		}
	    } # exists
	} # matches
    } # matches
} # while

close $rfh;


printRTROUT();
