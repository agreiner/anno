#!/usr/bin/perl

=pod

=head1 NAME

extractJobs.cname.pl

=head1 SYNOPSIS

./extractJobs.cname.pl <systemname> apevent.list hostlist jobs.out
./load_generic.py jobs.out <database> jobs ';'

=head1 DESCRIPTION

For Cray systems, extracts job information from I<alps> I<apevent> log files
and converts it into the format necessary to load into
the I<jobs> table. Note that it takes the native nid list format
and converts it to individual cnames in json format.


=head1 INSTRUCTIONS

=over

=item 1. ./extractJobs.cname.pl <systemname> apevent.list hostlist jobs.out
where I<systemname> is your system name, e.g., mutrino; I<apevent.list>
is the apevent log file output from alps; I<hostlist> is the I</etc/hosts>
file from your system; and I<jobs.out> is the output.

=item 2. Load I<jobs.out> into the I<jobs> table of your Jobs database, with delimeter ';'.

=back

=head1 NOTES

=over

=item * This is for Cray systems only due to alps and cnames.

=item * The scripts are intended to be a guide for building the material
to be loaded into the tables. They are not production hardened for
arbitrary systems.

=back

=head1 SEE ALSO

load_generic.py, README_table_support.txt

=cut


use strict;
use IO::Handle;

$|;

#extract the job data from alps files
#2015-03-27T14:13:13.406493-07:00 16311 EVENT[start]: apid 22820 uid 20983 cmdName 's3d.x' batchID '' numNids 96 nids [12-47,76-111,140-147,160-175]
#2015-03-27T14:13:30.402199-07:00 16311 EVENT[end]: apid 22820 uid 20983 cmdName 's3d.x' batchID '' numNids 96 nids [12-47,76-111,140-147,160-175]
#2015-05-16T17:09:01.854866-06:00 16408 EVENT[start]: apid 172594 uid 14560 cmdname 'doit' batchID '' nids 100 [12-47,76-111,140-147,160-179]
#2015-05-16T17:11:38.869130-06:00 16408 EVENT[sync][0]: apid 172594 uid 14560 cmdname 'doit' batchID '' nids 100 [12-47,76-111,140-147,160-179]

#nid list must be like: nid[02516-02575,02580-02635,02836].

my %jobHash;
my $delim = ';';

my $system = $ARGV[0];
my $apeventlist = $ARGV[1];
my $hostlist = $ARGV[2];
my $outfile = $ARGV[3];
open (my $fd, "<", $apeventlist) || die "Cannot open aprun list '$apeventlist'";
open (my $ofd, ">", $outfile) || die "Cannot open outfile '$outfile'";
$ofd->autoflush;

my %nid2cname;
open (my $hlist, "<", $hostlist) || die "Cannot open hostlist '$hostlist'";
while(<$hlist>){
    chomp;
    my $line = $_;
    $line =~ s/^\s+|\s+$//g;
    my @vals = split(/\s+/, $line);
    $nid2cname{$vals[1]} = $vals[2];
}
close $hlist;


sub formatnidlist{
    my ($inlist) = @_;

    my $llist;
    if ($inlist =~ /\[(.*)\]/){
	$llist = $1;
    } else {
	die "Bad nidlist '$inlist'\n";
    }

    my $retstr = "";
    my @ranges = split(/,/,$llist);
    foreach my $range (@ranges){
	if ($range =~ /(\d+)-(\d+)/){
	    my $min = $1;
	    my $max = $2;
	    for (my $i = $min; $i <= $max; $i++){
		my $temp = sprintf("nid%05d",$i);
		my $newtemp = $nid2cname{$temp};
		$retstr .= "\"$newtemp\",";
	    }
	} else {
	    my $temp = sprintf("nid%05d",$range);
	    my $newtemp = $nid2cname{$temp};
	    $retstr .= "\"$newtemp\",";
	}
    }

    #eat the comma
    my $rrstr = substr($retstr, 0, -1);
    my $rrstrx = "[" . $rrstr . "]";

    return $rrstrx;

}

print $ofd "JobId;Start;End;JobName;UID;NNodes;NodeList;System\n";

while (<$fd>){
    chomp;
    my $fname = $_;

    open (my $xfd, "<", $fname) || die "Cannot open aprun file '$fname'";
    my $count = 0;
    while(<$xfd>){
	chomp;
	my $line = $_;
	if ($line =~ /EVENT\[start\]/){
	    my  @vals = split(/\s+/, $line);
	    my $start = $vals[0];
	    my $jid = $vals[4];
	    my $uid = $vals[6];
	    my $nam = $vals[8];
	    my $numnids = $vals[12];
	    my $nidlist = $vals[scalar(@vals)-1];

	    my $currjid = $jid;
	    my $repeat = 0;
	    while (exists ($jobHash{$currjid})){
		$repeat++;
		$currjid = $jid . "." . $repeat;
#		print "Duplicate job '$jid'. Oldstart=$jobHash{$jid}{start}. Newstart = $start. Checking <$currjid>\n";
	    }
	    $jobHash{$currjid} = {
		jid => $jid,
		start => $start,
		end => -1,
		uid => $uid,
		name => $nam,
		numnids => $numnids,
		nidlist => $nidlist,
		file => $fname
	    };
	}
	if ($line =~ /EVENT\[end\]/){
	    my @vals = split(/\s+/, $line);
	    my $jid = $vals[4];
	    my $end = $vals[0];

	    my $currjid = $jid;
	    my $repeat = 0;
	    while (exists ($jobHash{$currjid})){
		if ($jobHash{$currjid}{end} != -1){
		#    print "\tWill skip $currjid and continue: $jobHash{$currjid}{end}\n";
		} else {
#		    print "\tWill break and fill in $currjid: $jobHash{$currjid}{end}\n";
		    last;
		}

		$repeat++;
		$currjid = $jid . "." . $repeat;
	    }

	    $jobHash{$currjid}{end} = $end;

	    # change format of start/end times. 2015-03-27T14:13:13.406493-07:00
	    if ($jobHash{$currjid}{start} =~ /(\d\d\d\d-\d\d-\d\d)T(\d\d:\d\d:\d\d)/){
		$jobHash{$currjid}{start} = "$1 $2";
	    }
	    if ($jobHash{$currjid}{end} =~ /(\d\d\d\d-\d\d-\d\d)T(\d\d:\d\d:\d\d)/){
		$jobHash{$currjid}{end} = "$1 $2";
	    }

	    my $newlist = formatnidlist($jobHash{$currjid}{nidlist});
	    print $ofd "$currjid;$jobHash{$currjid}{start};$jobHash{$currjid}{end};$jobHash{$currjid}{name};$jobHash{$currjid}{uid};$jobHash{$currjid}{numnids};$newlist;$system\n";
	}
    }
    close($xfd);
}
close($fd);
close($ofd);
