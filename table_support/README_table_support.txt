=pod

=head1 NAME

README_table_support.txt

=head1 SYNOPSIS

Documentation about the Annotation Database support tables and scripts.

=head1 DESCRIPTION

The I<Annotations> database has a number of supporting tables, in addition to
the I<annotations> table itself, that are used to determine components and their
relationships that will faciliate discovery and understanding of related
events.

High-level descriptions for developing data for each of these tables
are described here, along with scripts that faciliate extracting that
data and populating the tables.

=head2 Prerequisites:

Some information in the tables is not currently used in the annotations query tools,
for example, while there exist commands to extract associations based on the
physical component heirarchy, there are none that trace the link connectivity, at this time.
Therefore, at this time, it is only necessary that the I<physicalcomponents>, I<annotations>, I<authors>, 
I<LDgroups>, and I<componenttypes> tables in the Annotations database and the I<jobs> table 
in the Jobs database be populated.

Physical components information can be obtained from either I</etc/hosts>,
which gives you hostnames, and/or network information, which gives you the Gemini or Aries router
information, from which possible hosts can be inferred. The network information can be
obtained from the following commands, run on the smw: <rtr --Im>, which outputs the
network system map, or I<rtr --interconnect>, which outputs the network connectivity.

Even if you do not use the scripts for building your data, note that
lists of components need to be in json format, e.g., ["A","B","C"]
for the annotation query tool to parse them properly.

=over

=item * Creating Tables: You can use the commands in I<createAnnotationsDB.txt> and  I<createJobsDB.txt> interactively in sqlite3
to build the tables.

=item * Loading Tables: I<load_generic.py> can be used to load formatted files into an
sqlite3 table. Reference tables can be loaded by using I<populateTables.txt> or similar SQL 
customized for your architecture.

=head2 Primary Tables:

=item * I<jobs> table and database: The job information belongs the I<jobs> table in a 
separate Jobs database. Scheduler output, such as that from slurm or alps, can be used to 
determine the information.

For systems running alps, extract job information from the I<apevent.list> files
using I<extractJobs.cname.pl> and then load the output, I<jobs.out>, into the I<jobs> table using
I<load_generic.py>.

=item * I<physicalcomponents> table: The I<physicalcomponents> table contains information on
parent-child relationships of all the components, so that effects on related components may
be discovered. Components are specified by a
canonical name in this table (cnames for Cray systems), with aliases specified in the
I<alias> table.

Parent-child and other types of relationships are specified in the I<associationtypes>
table.

Parameters for your system should be
specified in I<createPhysicalTopology.pl>, and the output I<archPhysical.out> loaded into
the I<physicalcomponents> table using I<load_generic.py>.

=item * I<SUP> table: The I<SUP> table contains information on components that may affect
all other components. This is most frequently the smw, which is autopopulated in the
I<createTable.txt> commands, but other components can be added, for instance if you
are including facilities components.

=item * I<rtr> table: The I<rtr> table contains information on routers, such as the
associated blade and nics and network topology indicators. This table, along with
the I<links> table represents the network topology information.

There are two options for generating this input:
1) The systemmap, output by C<rtr -Im> is used as input to I<systemMapExtractor.pl> or
2) I</etc/hosts>  and the output of C<rtr --interconnect> are used as input to
I<hostInterconnectExtractor.pl>.
In either case, output is produced for the I<rtr> and I<alias> tables. These outputs
are loaded using I<load_generic.py>.

=item * I<links> table: The I<links> table contains information on the endpoints of
links. This table, along with the I<rtr> table, represents the network topology information.

The connectivity information, output by C<rtr --interconnect> is used as input to
I<interconnectExtractor.pl>. The output produced is loaded into the I<links> table
using I<load_generic.py>

=item * I<alias> table: The I<alias> table contains information on alternate names
for components. The canonical name for Cray components is the cname.

There are two options for generating this input:
1) The systemmap, output by C<rtr -Im> is used as input to I<systemMapExtractor.pl> or
2) I</etc/hosts>  and the output of C<rtr --interconnect> are used as input to
I<hostInterconnectExtractor.pl>.
In either case, output is produced for the I<rtr> and I<alias> tables. These outputs
are loaded using I<load_generic.py>.

Only the cname-to-nid alias is extracted in either case; additional aliases, such as
lnet nodes, wil have to be added manually. These are identifiable from I</etc/hosts>
but not from the systemmap.

=item * I<annotations> table: Annotations are generally manually made based on known
or discovered
log lines of interest. Tools such as Baler, Splunk, or even grep, which
extract log lines from log files can be used to reduce the overhead
of examining all the raw log data and can facilitate building input to
the annotations tables (e.g., extracting timestamped occurrences of
events). Details of these methods are beyond the scope of this document,
however there is a field in the table for the Baler Pattern Id, since many of the
release datasets used this method. As an example, however,
we include I<buildP0Annotation.pl> that takes a list of the occurrences of the
I<p0> directories, which are created on system reboots, and turns them
into annotations.

Users should examine the fields of the I<annotations> table. Beyond the
annotation description, information such as relevant components and domain
of events should also be specified. These require cross-referencing with
tables such as the I<physicalcomponents> and I<LDGroups> tables, among
others.

If a component cannot be associated with an annotation, the components field should
be empty (as opposed to an empty list).

Start and End times cannot be empty.  For a discrete event, set the end time
equal to the start time.

The fields starting with LD are not yet fully used. They
are intended to enable categorization of the patterns in alignment
with the LogDiver tool's categories. Details on this are beyond
the scope of this work.

=item * I<metaannotations> table:
Metaannotations tie together related annotations with some description of their relationship. MetaAnnotations are generally made manually based on non-log events or
groups of events. For example, a metaannotation can be used to
indicate a set of annotations related to facilities testing, or
fault injection tests.

References to annotations are specified as authorid + annotation id.
For example ["acb123","acb125","xyz123"]

=item * I<authors> table:
This table can store the 3 letter indicators for annotation authors.

=back

=head2 Reference Tables

In addtion to the above, there are other tables that are autopopulated by the i<populateTables.txt>
commands and generally include type information referenced in the other tables.

=over

=item * I<LDgroups> table: This is a set of possible types of domains to which an
annotation might pertian, for example network vs power. This is used for querying for events in
a domain and how events may propogate
from one domain to another (e.g., facilities events causing node failures). The group names come
from the LogDiver tool.

=item * I<componenttypes> table: This is a set of possible types of components.

=item * I<architectures> table: This table specifies architectures which determine relationships.
For example, physical relationships (in the I<physicalcomponents> table)  vs network relationships,
where the latter has been expressed
as router (in the I<rtr> table) as opposed to link (in the I<links> table)relationships.

=item * I<associationtypes> table: Specifies component associations, such as parent-child,
supremum, and router-tile, among others. Not all relationships are currently used.

=item * I<linktypes> table: These specify link types, and are populated with Gemini and Aries
options (e.g., X plus, X minus, green, black, etc)

=back

=head1 NOTES

=over

=item * All documentation is in perldoc. Individual scripts have their own documentation.
On OS X run perldoc -tT <filename> for it to display better.

=item * The scripts are intended to be a guide for building the material
to be loaded into the tables. They are not production hardened for
arbitrary systems.


=back

=head1 SEE ALSO
buildPOAnnotation.pl, createPhysicalTopology.pl, createAnnotationsDB.txt, createJobsDB.txt, extractJobs.cname.pl, hostInterconnectExtractor.pl,
interconnectExtractor.pl, load_generic.py, systemMapExtractor.pl.

  

=head1 Copyright Notice

'Anno' Copyright (c) 2018, The Regents of the University of California, through Lawrence Berkeley National Laboratory, and National Technology & Engineering Solutions of Sandia, LLC, for the operation of Sandia National Laboratory (subject to receipt of any required approvals from the U.S. Dept. of Energy). All rights reserved.

If you have questions about your rights to use or distribute this software, please contact Berkeley Lab's Intellectual Property Office at IPO@lbl.gov.

NOTICE. This Software was developed under funding from the U.S. Department of Energy and the U.S. Government consequently retains certain rights. As such, the U.S. Government has been granted for itself and others acting on its behalf a paid-up, nonexclusive, irrevocable, worldwide license in the Software to reproduce, distribute copies to the public, prepare derivative works, and perform publicly and display publicly, and to permit other to do so.

=cut
