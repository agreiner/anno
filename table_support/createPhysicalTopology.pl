#!/usr/bin/perl

use strict;

=pod

=head1 NAME

createPhysicalTopology.pl

=head1 SYNOPSIS

./createPhysicalTopology.pl > archPhysical.out
./load_generic.py ./archPhysical.pl <db> physicalcomponents ';'

=head1 DESCRIPTION

For Cray systems, this builds parent-child information for the components.
Use information about the number of rows and columns of cabs as parameters in this file.
The output will be input to the I<physicalcomponents> table.


=head2 INSTRUCTIONS

=over

=item 1. Edit the parameters for your system (e.g., minRow, maxRow, architecture (G or A). Examples
are in the script. Be sure to comment out all others.

=item 2. ./createPhysicalTopology.pl > archPhysical.out

=item 3. Load into the I<physicalcomponents> table of your Annotations database, with delimeter ';'.

=back

=head1 NOTES

=over

=item * This is for Cray Gemini and Aries based systems only.

=item * We define (for example) C<c0-0c0s0a0n0> to refer to the NIC and it is a child of the router.

=item * Components are specified by a canonical name (cnames for Cray systems), with aliases
identified in the I<alias> table.

=item * For components without parents or children, represent those as empty lists

=item * The scripts are intended to be a guide for building the material
to be loaded into the tables. They are not production hardened for
arbitrary systems.


=back

=head1 SEE ALSO

README_table_support.txt, load_generic.py

=cut


# component types
#NDE|node
#BLD|blade
#CHA|chassis
#CAB|cabinet
#RTR|router
#LNK|link
#TLE|tile
#NIC|nic
#SMW|smw
#OTH|other


# INSTRUCTIONS -- Set up the parameters for your system. Also if its gemini or aries
# Make sure all others are commented out

my $minRow;
my $maxRow;
my $minCol;
my $maxCol;
my $arch;
my $routerPerBlade;
my $NICPerRouter;
my $nodePerBlade;
my $bladePerChassis;
my $chassisPerRack;
my $tileMaxRow;
my $tileMaxCol;


# note -- even if some cabs DNE or are not fully popoulated, this will put them in, but thats ok
#----------------------------#
# EDIT HERE
#----------------------------#
# Cielo
$minRow=0;
$maxRow=15;
$minCol=0;
$maxCol=5;
$arch= "G";


#Tr2
#$minRow=0;
#$maxRow=11;
#$minCol=5;
#$maxCol=9;
#$arch= "A";

#Mutrino (2 cab)
#$minRow=0;
#$maxRow=0;
#$minCol=0;
#$maxCol=0;
#x$arch = "A";

#------------------------------#


if ($arch eq "G"){
    $routerPerBlade=2;
    $NICPerRouter=2;
    $nodePerBlade=4;
    $bladePerChassis=8;
    $chassisPerRack=3;
    $tileMaxRow=5;
    $tileMaxCol=7;
} elsif ($arch eq "A") {
    $routerPerBlade=1;
    $nodePerBlade=4;
    $NICPerRouter=4;
    $bladePerChassis=16;
    $chassisPerRack=3;
    $tileMaxRow=5;
    $tileMaxCol=7;
} else {
    die "Bad arch: '$arch'\n";
}



my $uid = 0;
my %HoH;


sub printComponents(){
    print "id;type;typenum;cname;parent;children\n";
    my @sortedkeys = sort { $HoH{$a}{uid} <=> $HoH{$b}{uid} } keys(%HoH);
    foreach  my $comp ( @sortedkeys){
	print "$HoH{$comp}{uid};";
	print "$HoH{$comp}{type};";
	print "$HoH{$comp}{typenum};";
	print "$HoH{$comp}{name};";

	my @arr = @{$HoH{$comp}{parent}};
	print "\[";
	for (my $i = 0; $i < scalar(@arr);$i++){
	    print "\"" . $arr[$i] . "\"";
	    if ($i < scalar(@arr)-1){
		print ",";
	    }
	}
	print "];";

	my @arr = @{$HoH{$comp}{children}};
	print "\[";
	for (my $i = 0; $i < scalar(@arr);$i++){
	    print "\"" . $arr[$i] . "\"";
	    if ($i < scalar(@arr)-1){
		print ",";
	    }
	}
	print "]";
	print "\n";
    }
}


#special components
$HoH{ smw } = {
    uid => $uid++,
    name => "smw",
    type => "SMW",
    typenum => 0,
    parent => [],
    children => []
};

my $nrack = 0;
for (my $row = $minRow; $row <= $maxRow; $row++){
    for (my $col = $minCol; $col <=$maxCol; $col++){

	#cabinet
	my $rack = "c" . $row . "-" . $col;
	push(@{$HoH{smw}{children}}, $rack);

	$HoH{ $rack } = {
	    uid => $uid++,
	    name => $rack,
	    type => "CAB",
	    typenum =>$nrack++,
	    parent => ["smw"],
	    children => []
	};

	my $nchassis = 0;
	#chassis
	for (my $ch = 0; $ch < $chassisPerRack; $ch++){
	    my $chassis = $rack . "c" . $ch;

	    push(@{$HoH{$rack}{children}}, $chassis);

	    $HoH{ $chassis } = {
		uid => $uid++,
		name => $chassis,
		type => "CHA",
		typenum => $nchassis++,
		parent => [$rack],
		children => []
	    };


	    #blade
	    my $nblade = 0;
	    for (my $bl = 0; $bl < $bladePerChassis; $bl++){
		my $blade = $chassis . "s" . $bl;

		push(@{$HoH{$chassis}{children}}, $blade);

		$HoH{ $blade } = {
		    uid => $uid++,
		    name => $blade,
		    type => "BLD",
		    typenum => $nblade++,
		    parent => [$chassis],
		    children => []
		};

		#router
		my $nrouter = 0;
		for (my $rtr = 0; $rtr < $routerPerBlade; $rtr++){

		    my $router;
		    if ($arch eq "G"){
			$router = $blade . "g" . $rtr;
		    } else {
			$router = $blade . "a" . $rtr;
		    }

		    push(@{$HoH{$blade}{children}}, $router);

		    $HoH{ $router } = {
			uid => $uid++,
			name => $router,
			type => "RTR",
			typenum => $nrouter++,
			parent => [$blade],
			children => []
		    };

		    #tiles
		    my $ntile = 0;
		    for (my $tr = 0; $tr <= $tileMaxRow; $tr++){
			for (my $tc = 0; $tc <= $tileMaxCol; $tc++){
			    my $tile = $router . "l" . $tr . $tc;

			    push(@{$HoH{$router}{children}}, $tile);

			    $HoH{ $tile } = {
				uid => $uid++,
				name => $tile,
				type => "TLE",
				typenum => $ntile++,
				parent => [$router],
				children => []
			    };
			}
		    }

		    my $minnic = 0;
		    my $nnic = 0;
		    #NICS - WELL KNOWN
		    if ($arch eq "G"){
			if ($rtr == 1){
			    $minnic = $NICPerRouter;
			}
		    }
		    for (my $nn = $minnic; $nn < $NICPerRouter*($rtr+1); $nn++){
			my $nic = $router . "n" . $nn;

			push(@{$HoH{$router}{children}}, $nic);

			$HoH{ $nic } = {
			    uid => $uid++,
			    name => $nic,
			    type => "NIC",
			    typenum => $nnic++,
			    parent => [$router],
			    children => []
			};
		    }
		}

		#nodes
		my $nnode = 0;
		for (my $nd = 0; $nd < $nodePerBlade; $nd++){
		    my $node = $blade . "n" . $nd;

		    push(@{$HoH{$blade}{children}}, $node);

		    $HoH{ $node } = {
			uid => $uid++,
			name => $node,
			type => "NDE",
			typenum => $nnode++,
			parent => [$blade],
			children => []
		    };

		}
	    }

	}
    }
};



printComponents;
