#!/usr/bin/env python

# ABOUT: use this to load data into the database tables
# NOTE: the arguments are (in order) <filetoload> <database> <table> <delimiter>
# NOTE: some args for delimiter may not be able to parsed on the command
#       line, and can instead be wired into the code if necessary.
# NOTE: The scripts are intended to be a guide for building the material
#       to be loaded into the tables. They are not production hardened for
#       arbitrary systems.


import json
import sys
import csv
import sqlite3

filepath = sys.argv[1]
db = sys.argv[2]
table = sys.argv[3]
chardelimiter = sys.argv[4]
values = []
with open(filepath, newline='') as csvfile:
  filereader = csv.reader(csvfile,delimiter=chardelimiter)
  headers = next(filereader)
  qmarks = []
  for i in range(0, len(headers)):
    qmarks.append("?")
  qstring = (",").join(qmarks)

  for row in filereader:
    values.append(tuple(row))

insertstr = "insert into " + table + "('" + ("', '").join(headers) + "') values(" + qstring + ")"
print(insertstr)
print(values)
conn = sqlite3.connect(db)
conn.isolation_level = None
c = conn.cursor()
for tuple in values:
  c.execute(insertstr, tuple)
  print(tuple)

#c.executemany(insertstr, values)
conn.commit
conn.close()
	#more checks to come
	# wait, lets look at pandas for this
