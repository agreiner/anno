# Anno
**A query engine and toolset for HPC log annotations.**

The Anno log annotation system enables researchers, staff, and users of high-performance computing systems to make sense of the overwhelmingly large sets of log data produced by complex computing systems. Investigators interested in mining log files for insight into the behaviors of such systems can use it to understand what they see in the logs and follow failures as they propagate. System administrators as well as staff responsible for networking, storage, job management, and other aspects of these large systems can collaborate by entering annotations and sharing them. The system can help to answer questions relating to a specific job's performance or a particular failure or error.

 Anno provides tools for creating and querying annotations to supercomputing log data. The initial distribution includes a query tool (anno.py), a set of processing scripts, and a standardized database schema. The latter enables annotations from diverse sources to be analyzed together. The processing scripts can be used for data preparation tasks. These include taking data from system and job log files and transforming it into the databases needed by the query tool, for automatically generating useful annotations from log data, and for importing manually created annotations into the system. The query tool provides options for retrieving annotations that meet user criteria, including those that apply to a particular physical component or connected components, those identified with a particular time range, those that immediately follow a certain event time, those relevant to a particular computation, and those that contain a particular phrase or string. 

##Requirements and Installation

For querying the data with anno.py, you will need

* Python version 3
* SQLite 3

If you want to create your own annotations, scripts in the ingest_tools directory can be used with 

* Python 3
* SQLite 3
* SQLAlchemy

If you want to set up your own data for analysis with Anno, you can use the scripts in the table_support directory along with

* Perl 5
* SQLite 3

### Anno.py Setup

To begin working with data, the anno.py python application needs access to two databases, one with the actual annotations and one with job data. There are two ways of pointing the tool to your databases. First, you can simply place copies of your annotations and jobs databases into the same directory as anno.py and name them 'annotations.sqlite3' and 'jobs.sqlite3'. Second, you can edit the anno.conf file to point to the databases wherever they are located on your system. For example, you can uncomment the lines beginning with anno_db_path and job_db_path and set them like the following:

`anno_db_path='/path/to/databases/myAnnotations.sqlite3'`

`job_db_path='/path/to/databases/myJobs.sqlite3'`

If you uncomment paths in anno.conf, anno.py will use those paths rather than the annotations.sqlite3 and jobs.sqlite3 databases.

## Using Anno.py

### Example Queries

Retrieve annotations relevant after a certain **start** time (Anno will attempt to interpret any date format. Be sure to use quotes around values with a space.)  
`./anno.py -s '2018-03-20 00:00:00'`  
`./anno.py -s 20180320T00:00:00`

Retrieve annotations before a certain **end** time  
`./anno.py -e 20180320T000000`

Retrieve annotations during a time range by using both **start and end** times  
`./anno.py -s '2018-03-20 00:00:00' -e '2018-03-20 11:00:00'`  
`./anno.py -s 20180320T000000 -e 20180320T110000`

Retrieve the **next** single annotation after a certain time point  
`./anno.py -n 20180320T00:00:00`

Retrieve annotations about a **component** (Components can be identified by a canonical name or an alias in the alias table.)  
`./anno.py -c 'c0-0c0s1n6'`  
`./anno.py -c 'nid0000016'`

Retrieve annotations about a component and other components connected to it in the hierarchy of physical components, to a given **depth**  
`./anno.py -c 'c0-0c0s1n6' -d 1'`

Retrieve annotations about a **type** of issue, based on a specific word or phrase  
`./anno.py -t 'node down'`

Combinations of the above, e.g., the next single annotation of a certain type after a certain time point  
`./anno.py -t 'node down' -n '2018-03-20 11:51:00'`

Retrieve annotations related to a **jobid**  
`./anno.py -j 9847125`

All the above in **json** format  
`./anno.py -j 9847125 -f json`

All the above in **table** format  
`./anno.py -j 9847125 -f table`

Retrieve **Jobs** related to a given annotation, by the annotation identifier  
`./anno.py --jobs AMG1234`

Retrieve the result of an **SQL*** query on the annotations or jobs database (Use double quotes for quotes within the query.)  
`./anno.py -q annotations 'select ** from annotations where authorid="AMG"'`

Print the **help** text  
`./anno.py -h`

### Additional Flags

`-l, --limit`  
Limit the output to the specified number of rows from the database

`--nosmw`  
By default, depth queries include the system management workstation, or SMW, or supremum, as a component. We include it because most annotations that apply to that workstation also apply to the entire machine. You can use this flag to leave out annotations about the SMW that would not otherwise match your query.

`--nullc`  
By default, annotations with no specifiec component are left out of queries by component. If you want to include annotations that have no component assigned in the result of a component query, you can use this flag.

`-v`  
This flag increases the verbosity of the output, mostly for debugging

`-a, --all`  
Running a query with no flags will return the help text rather than all the annotations in the database. If you really want to retrieve all the annotations in the database, you can use this flag alone.



### Reference

#### Usage: 

~~~~
anno.py [-h] [-s STARTING] [-e ENDING] [-c COMPONENT] [-t TYPE]
               [-n NEXT] [-j JOBID] [-f FORMAT] [-d DEPTH] [-v] [-q SQL SQL]
               [-l LIMIT] [--jobs JOBS] [--nosmw] [--nullc] [-a]
~~~~

Query the annotations for a logset.

#### Optional arguments:
`-h, --help`  
show this help message and exit
  
`-s STARTING, --starting STARTING`  
A start time that limits retrieved annotations to those relevant to the time at or after it, e.g., '2017-01-01 12:00:00'. When used in conjunction with --jobid, overrides the job start time.

`-e ENDING, --ending ENDING`  
An end time that limits retrieved annotations to those relevant to the time at or before it, e.g., '2017-01-01 12:59:59'. When used in conjunction with --jobid, overrides the job end time.

`-c COMPONENT, --component COMPONENT`  
A component identifier. When used in conjunction with --jobid, overrides the job's list of components.

`-t TYPE, --type TYPE`  
A string describing a type of event

`-n NEXT, --next NEXT`  
A time after which to find the next single annotation with a start time after the given time and that otherwise matches the query, e.g., '2017-01-01 12:00:00'

`-j JOBID, --jobid JOBID`  
A job id from the scheduler

`-f FORMAT, --format FORMAT`  
How to format the output, table, json, or default

`-d DEPTH, --depth DEPTH`  
How many levels to traverse to find related components. This has no effect without a component or job flag. The system management workstation is included by default.

`-v, --verbose`  
Be noisier. -vv is noisier than -v, etc
  
`-q SQL SQL, --sql SQL SQL`  
 A database and an sql query to run against it. The first argument is the database (jobs or annotations). The second is the query, in single quotes. Use double quotes to delimit strings within the query.

`-l LIMIT, --limit LIMIT`  
Limit the number of output records

`--jobs JOBS`  
Show which jobs were running during the time spanned by this annotation. Requires annotator initials and annotation id, e.g., AMG123.

`--nosmw`  
Override default addition of system management workstation as a component in component queries with depth specified

`--nullc`  
Include annotations that do not contain a component in results from component queries with depth specified

`-a, --all`  
Return all annotations in the database (may be very large)  
  



## Using Anno with Your Own Data

For those who have their own data and would like to be able to use Anno to query it, we provide a set of scripts that make the necessary data transformations easier. These are written in Perl and housed in the table_support directory. Setting up your data requires creating an Annotations database and a Jobs database with the appropriate schemas.

The Annotations database has a number of supporting tables, in addition to
the _annotations_ table itself, that are used to determine components and their
relationships that will faciliate discovery and understanding of related
events.

High-level descriptions for developing data for each of these tables
are described here, along with scripts that faciliate extracting that
data and populating the tables.

### Prerequisites:

Some information in the tables is not currently used in the annotations query tools,
for example, while there exist commands to extract associations based on the
physical component heirarchy, there are none that trace the link connectivity at this time.
Therefore, at this time, it is only necessary that the _physicalcomponents_, _annotations_, _authors_, _LDgroups_, and _componenttypes_ tables
in the Annotations database and the _jobs_ table in the Jobs database be populated.

Physical components information can be obtained from either _/etc/hosts_,
which gives you hostnames, and/or network information, which gives you the Gemini or Aries router
information, from which possible hosts can be inferred. The network information can be
obtained from the following commands, run on the smw: `rtr --Im`, which outputs the
network system map, or `rtr --interconnect`, which outputs the network connectivity.

Even if you do not use the scripts for building your data, note that
lists of components need to be in json format, e.g., ["A","B","C"]
for the annotation query tool to parse them properly.

Creating Tables: You can use the commands in _createAnnotationsDB.txt_ and _createJobsDB.txt_ interactively in sqlite3 to build the tables.

Loading Tables: _load_generic.py_ can be used to load formatted files into an
sqlite3 table. Reference tables can be loaded by using _populateTables.txt_ or similar SQL customized for your architecture.

### Primary Tables:

* _jobs_ table in Jobs database: The job information belongs in the _jobs_ table in a separate Jobs database. Scheduler
output, such as that from slurm or alps, can be used to determine
the information.  
For systems running alps, extract job information from the _apevent.list_ files
using _extractJobs.cname.pl_ and then load the output, _jobs.out_, into the _jobs_ table using
_load_generic.py_.

* _physicalcomponents_ table: The _physicalcomponents_ table contains information on
parent-child relationships of all the components, so that effects on related components may
be discovered. Components are specified by a
canonical name in this table (cnames for Cray systems), with aliases specified in the
_alias_ table.  
Parent-child and other types of relationships are specified in the _associationtypes_
table.  
Parameters for your system should be
specified in _createPhysicalTopology.pl_, and the output _archPhysical.out_ loaded into
the _physicalcomponents_ table using _load_generic.py_.

* _SUP_ table: The _SUP_ table contains information on components that may affect
all other components. This is most frequently the smw, which is autopopulated in the
_createTable.txt_ commands, but other components can be added, for instance if you
are including facilities components.

* _rtr_ table: The _rtr_ table contains information on routers, such as the
associated blade and nics and network topology indicators. This table, along with
the _links_ table represents the network topology information. There are two options for generating this input:
  1. The systemmap, output by `rtr -Im` is used as input to _systemMapExtractor.pl_ or
  2. _/etc/hosts_  and the output of `rtr --interconnect` are used as input to
_hostInterconnectExtractor.pl_.  
In either case, output is produced for the _rtr_ and _alias_ tables. These outputs
are loaded using _load_generic.py_.

* _links_ table: The _links_ table contains information on the endpoints of
links. This table, along with the _rtr_ table, represents the network topology information.  
The connectivity information, output by `rtr --interconnect` is used as input to
_interconnectExtractor.pl_. The output produced is loaded into the _links_ table
using _load_generic.py_.

* _alias_ table: The _alias_ table contains information on alternate names
for components. The canonical name for Cray components is the cname. There are two options for generating this input:
  1. The systemmap, output by `rtr -Im` is used as input to _systemMapExtractor.pl_ or
  2. I</etc/hosts>  and the output of C<rtr --interconnect> are used as input to
_hostInterconnectExtractor.pl_.  
In either case, output is produced for the _rtr_ and _alias_ tables. These outputs
are loaded using _load_generic.py_.  
Only the cname-to-nid alias is extracted in either case; additional aliases, such as
lnet nodes, wil have to be added manually. These are identifiable from _/etc/hosts_
but not from the systemmap.

* _annotations_ table: Annotations are generally manually made based on known
or discovered
log lines of interest. Tools such as Baler, Splunk, or even grep, which
extract log lines from log files can be used to reduce the overhead
of examining all the raw log data and can facilitate building input to
the annotations tables (e.g., extracting timestamped occurrences of
events). Details of these methods are beyond the scope of this document,
however there is a field in the table for the Baler Pattern Id, since many of the
release datasets used this method. As an example, however,
we include _buildP0Annotation.pl_ that takes a list of the occurrences of the
_p0_ directories, which are created on system reboots, and turns them
into annotations.  
Users should examine the fields of the _annotations_ table. Beyond the
annotation description, information such as relevant components and domain
of events should also be specified. These require cross-referencing with
tables such as the _physicalcomponents_ and _LDGroups_ tables, among
others.  
If a component cannot be associated with an annotation, the components field 
should be empty (as opposed to an empty list).  
Start and end times cannot be empty. For a discrete event, set the end time
equal to the start time.  
The fields starting with LD are not yet fully used. They
are intended to enable categorization of the patterns in alignment
with the LogDiver tool's categories. Details on this are beyond
the scope of this work.

* _metaannotations_ table:
Metaannotations tie together related annotations with some description of their relationship. MetaAnnotations are generally made manually based on non-log events or
groups of events. For example, a metaannotation can be used to
indicate a set of annotations related to facilities testing, or
fault injection tests.  
References to annotations are specified as authorid + annotation id.
For example ["acb123","acb125","xyz123"]

* _authors_ table:
This table can store the three-letter indicators for annotation authors.

### Reference Tables

In addition to the above, there are other tables that are autopopulated by the _populateTables.txt_
commands and generally include type information referenced in the other tables.

* _LDgroups_ table: This is a set of possible types of domains to which an
annotation might pertian, for example network vs power. This is used for querying for events in
a domain and how events may propogate
from one domain to another (e.g., facilities events causing node failures). The group names come from the LogDiver tool.

* _componenttypes_ table: This is a set of possible types of components.

* _architectures_ table: This table specifies architectures which determine relationships.
For example, physical relationships (in the _physicalcomponents_ table)  vs network relationships,
where the latter has been expressed
as router (in the _rtr_ table) as opposed to link (in the _links_ table)relationships.

* _associationtypes_ table: Specifies component associations, such as parent-child,
supremum, and router-tile, among others. Not all relationships are currently used.

* _linktypes_ table: These specify link types, and are populated with Gemini and Aries
options (e.g., X plus, X minus, green, black, etc)

### Notes

All documentation is in perldoc. Individual scripts have their own documentation.
On OS X run `perldoc -tT <filename>` for it to display better.

The scripts are intended to be a guide for building the material
to be loaded into the tables. They are not production hardened for
arbitrary systems.


See also buildPOAnnotation.pl, createPhysicalTopology.pl, createAnnotationsDB.txt, createJobsDB.txt, extractJobs.cname.pl, hostInterconnectExtractor.pl,
interconnectExtractor.pl, load_generic.py, systemMapExtractor.pl.
  


## Ingesting Your Own Annotations

The ingest_annotations directory includes two scripts to support ingesting annotations into an annotations database:

- **csv_template.py** writes the heading of a table into a csv file, which can
  then be imported into a spreadsheet for easier editing.

  Running `csv_template.py --help` will produce the following usage text:

~~~~
  csv_template.py [-h] [-f] [table] [outfilename]
  writes a CSV file with the headings for the selected table name
  this file can be manually edited and then ingested with ingest_csv.py
  The default table is 'annotations' and the default filename is <tablename>.csv'
  -f will overwrite an existing output file, if there is one

  Available tables are:
     annotations   LDgroups         metaannotations    authors
     jobs          componenttypes   associationtypes   linktypes
     architectures rtr              physicalcomponents link   
     alias
~~~~

- **ingest_csv.py** reads data from a CSV-format file into a table, checking and 
  cleaning table entries along the way. It currently supports the main 'annotations' 
  and 'jobs' tables - for other tables, using `load_generic.py` in the `table_support/` 
  directory is recommended. 

  The tool sanitizes dates, boolean fields, component aliases and some missing data.
  You can specify "constant" values for authorid or system for all entries in a 
  `field=value` format 

  Running `ingest_csv.py --help` will produce the following usage text:

~~~~
  ingest_csv.py [-h] [-t tablename] csvfile [field=value ...]
  reads a CSV file and ingests into the selected table
  The default table is 'annotations', which accepts global values
  for the fields authorid= and system=
  Currently supported tables are 'annotations' and 'jobs'. For other
  tables please use ../table_support/load_generic.py
~~~~


The ingest_annotations directory also has a `db_support.py` module, which provides the functionality used in these tools.  
  

# Copyright Notice

'Anno' Copyright (c) 2018, The Regents of the University of California, through Lawrence Berkeley National Laboratory, and National Technology & Engineering Solutions of Sandia, LLC, for the operation of Sandia National Laboratory (subject to receipt of any required approvals from the U.S. Dept. of Energy). All rights reserved.

If you have questions about your rights to use or distribute this software, please contact Berkeley Lab's Intellectual Property Office at IPO@lbl.gov.

NOTICE. This Software was developed under funding from the U.S. Department of Energy and the U.S. Government consequently retains certain rights. As such, the U.S. Government has been granted for itself and others acting on its behalf a paid-up, nonexclusive, irrevocable, worldwide license in the Software to reproduce, distribute copies to the public, prepare derivative works, and perform publicly and display publicly, and to permit other to do so.
